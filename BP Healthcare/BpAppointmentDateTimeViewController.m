//
//  BpAppointmentDateTimeViewController.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpAppointmentDateTimeViewController.h"

@interface BpAppointmentDateTimeViewController ()

@end

@implementation BpAppointmentDateTimeViewController
@synthesize datePicker;
@synthesize datelabel;
@synthesize gBpNewAppointmentViewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    BpAppointmentDateTimeViewController *gBpAppointmentDateTimeViewController=[[BpAppointmentDateTimeViewController alloc] initWithNibName:@"BpAppointmentDateTimeViewController" bundle:nil];
    /**/UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom]; 
    [leftButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];    
    leftButton.frame = CGRectMake(0, 0, 50, 33);
    [leftButton addTarget:self action:@selector(handleBack:) forControlEvents:UIControlEventTouchUpInside];   
    
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView: leftButton] autorelease]; 
    gBpAppointmentDateTimeViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom]; 
    [rightButton setImage:[UIImage imageNamed:@"btn_done.png"] forState:UIControlStateNormal];    
    rightButton.frame = CGRectMake(0, 0, 62, 33);
    [rightButton addTarget:self action:@selector(selectedDatetapped:) forControlEvents:UIControlEventTouchUpInside];   
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView: rightButton] autorelease]; 
    
    gBpAppointmentDateTimeViewController.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    
    [rightButton release];
    
    [leftButton release];
    [gBpAppointmentDateTimeViewController release];
    
    
    /*=======================*/
    //datePicker.datePickerMode =UIDatePickerModeDate;
    //[datePicker setDate:[NSDate date] animated:NO];
    datePicker.datePickerMode =UIDatePickerModeDateAndTime;
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    // now build a NSDate object for the next day
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setDay:3];
    
    //picker time set at 7:30 morning
    NSDate *date = [NSDate date];
    NSCalendar *gregorian2 = [[NSCalendar alloc] initWithCalendarIdentifier: NSGregorianCalendar];
    NSDateComponents *components = [gregorian2 components: NSUIntegerMax fromDate: date];
    [components setHour: 7];
    [components setMinute: 30];
    
    NSDate *newDate = [gregorian dateFromComponents: components];
    
    NSDate *nextDate = [gregorian dateByAddingComponents:offsetComponents toDate: newDate options:0];

    datePicker.minimumDate = nextDate;
    [datePicker addTarget:self action:@selector(LabelChange:) forControlEvents:UIControlEventValueChanged];
    [self LabelChange:datePicker];
    
    
}

- (void)viewDidUnload
{
    [self setDatePicker:nil];
    [self setDatelabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void) handleBack:(id)sender
{
    // do your custom handler code here
    
    // make sure you do this!
    // pop the controller
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)LabelChange:(id)sender
{
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    //[formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:3600*2]];
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:+3600*8]];
    [formatter setDateFormat:@"dd/MM/yyyy hh:mm a"];
    NSString* pickerDateStr = [formatter stringFromDate:[datePicker date]];
    NSDate* pickerDate = [formatter dateFromString:pickerDateStr];
    [formatter release];
    
	//NSDateFormatter *df = [[NSDateFormatter alloc] init];
   	//df.dateStyle = NSDateFormatterMediumStyle;
	//datelabel.text = [NSString stringWithFormat:@"%@", [df stringFromDate:datePicker.date]];
    datelabel.text = [NSString stringWithFormat:@"%@", pickerDateStr];
}

-(IBAction)selectedDatetapped:(id)sender
{
    NSString *errorMsg = @"";
    
    if([self checkAppointmentDayAvailable:&errorMsg]){
    
        UILabel *lblServiceTypeSelected=(UILabel*)[self.gBpNewAppointmentViewController lblAppointmentDate];
        
        lblServiceTypeSelected.text=datelabel.text;
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }else{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Warning!" message:errorMsg delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertView show];
        [alertView release];
    }

}

-(BOOL)checkAppointmentDayAvailable:(NSString **)resultMsg{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init] ;
    [dateFormatter setDateFormat:@"EEEE"];
    NSString *dayOfWeek = [dateFormatter stringFromDate:[datePicker date]];
    NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
    [timeFormat setDateFormat:@"HHmm"];
    NSInteger time = [[timeFormat stringFromDate:[datePicker date]]integerValue];
    
    NSLog(@"\n"
          "theDate: |%d| \n"
          , time);
    
    if([dayOfWeek isEqualToString:@"Monday"] || [dayOfWeek isEqualToString:@"Tuesday"] || [dayOfWeek isEqualToString:@"Wednesday"] || [dayOfWeek isEqualToString:@"Thursday"] || [dayOfWeek isEqualToString:@"Friday"]){
        if(time < 730 || time >= 1700 ){
            NSLog(@"time wrong");
            *resultMsg = @"Sorry, our business time on Monday to Friday is 7:30AM ~ 05:00PM.";
            return NO;
        }

    }else if([dayOfWeek isEqualToString:@"Saturday"]){
        if(time < 800 || time >= 1300 ){
            NSLog(@"time wrong");
            *resultMsg = @"Sorry, our business time on Saturday is 8:00AM ~ 01:00PM .";
            return NO;
        }
        
    }else if([dayOfWeek isEqualToString:@"Sunday"]){
        *resultMsg = @"Sorry, we close on sunday.";
        return NO;
    }else{
        
    }
    
    return YES;
}

-(void)getdate{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"MMM dd, yyyy HH:mm"];
    NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
    [timeFormat setDateFormat:@"HH:mm:ss"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init] ;
    [dateFormatter setDateFormat:@"EEEE"];
    
    NSDate *now = [[NSDate alloc] init];
    NSString *dateString = [format stringFromDate:now];
    NSString *theDate = [dateFormat stringFromDate:now];
    NSString *theTime = [timeFormat stringFromDate:[datePicker date]];
    
    NSString *week = [dateFormatter stringFromDate:[datePicker date]];
    
    NSLog(@"\n"
          "theDate: |%@| \n"
          "theTime: |%@| \n"
          "Now: |%@| \n"
          "Week: |%@| \n"
          
          , theDate, theTime,dateString,week);
}

- (void)dealloc 
{
    [gBpNewAppointmentViewController release];
    [datePicker release];
    [datelabel release];
    [super dealloc];
}
@end
