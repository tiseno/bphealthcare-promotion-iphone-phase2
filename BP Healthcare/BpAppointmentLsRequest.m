//
//  BpAppointmentLsRequest.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpAppointmentLsRequest.h"

@implementation BpAppointmentLsRequest
@synthesize tokenId;

-(id)initWithsTokenId:(NSString *)itokenId;
{
    self=[super init];
    if(self)
    {
        self.SOAPAction=@"retrieveAppointmentList";
        self.requestType=WebserviceRequest;
        self.tokenId=itokenId;
    }
    return self;
}

-(void)dealloc
{
    [tokenId release];
    [super dealloc];
}

-(NSString*) generateHTTPPostMessage
{
    
    NSString *postMsg = [NSString stringWithFormat:@"tokenId=%@",self.tokenId];
    return postMsg;
} 

@end
