//
//  BpAppointmentTypeDescViewController.h
//  BP Healthcare
//
//  Created by desmond on 13-1-30.
//
//

#import <UIKit/UIKit.h>

@interface BpAppointmentTypeDescViewController : UIViewController
@property (retain, nonatomic) IBOutlet UIView *contentView;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollView;

@end
