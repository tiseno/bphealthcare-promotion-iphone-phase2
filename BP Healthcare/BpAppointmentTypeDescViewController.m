//
//  BpAppointmentTypeDescViewController.m
//  BP Healthcare
//
//  Created by desmond on 13-1-30.
//
//

#import "BpAppointmentTypeDescViewController.h"

@interface BpAppointmentTypeDescViewController ()

@end

@implementation BpAppointmentTypeDescViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    BpAppointmentTypeDescViewController *gBpAppointmentTypesViewController=[[BpAppointmentTypeDescViewController alloc] initWithNibName:@"BpAppointmentTypeDescViewController" bundle:nil];
    /**/UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton setImage:[UIImage imageNamed:@"btn_close.png"] forState:UIControlStateNormal];
    leftButton.frame = CGRectMake(0, 0, 62, 33);
    [leftButton addTarget:self action:@selector(handleBack:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView: leftButton] autorelease];
    gBpAppointmentTypesViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    
    if ([self.navigationController.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)] ) {
        UIImage *image = [UIImage imageNamed:@"bar_top.png"];
        [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    }
    
    [leftButton release];
    [gBpAppointmentTypesViewController release];
    
    CGSize size = _contentView.frame.size;
    [_scrollView setContentSize:size];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) handleBack:(id)sender
{
    [self dismissModalViewControllerAnimated:YES];
}

- (void)dealloc {
    [_contentView release];
    [_scrollView release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setContentView:nil];
    [self setScrollView:nil];
    [super viewDidUnload];
}
@end
