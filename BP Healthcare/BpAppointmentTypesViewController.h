//
//  BpAppointmentTypesViewController.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "BpAppointmentTypeDescViewController.h"
//#import "NetworkHandler.h"

@class BpNewAppointmentViewController;

@interface BpAppointmentTypesViewController : UIViewController<UIAlertViewDelegate>{
    
}

@property (retain, nonatomic) IBOutlet UITableView *tblApptypes;
@property (nonatomic, retain) BpNewAppointmentViewController *gBpNewAppointmentViewController;
@property (retain, nonatomic) NSArray *AppointmentTypesArr;
@property (retain, nonatomic) NSString *SelectedBranch;
@property (retain, nonatomic) NSIndexPath *lastIndexPath;

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;

@end
