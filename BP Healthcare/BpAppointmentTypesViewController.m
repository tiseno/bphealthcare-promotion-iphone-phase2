//
//  BpAppointmentTypesViewController.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpAppointmentTypesViewController.h"

@interface BpAppointmentTypesViewController ()

@end

@implementation BpAppointmentTypesViewController
@synthesize tblApptypes;
@synthesize gBpNewAppointmentViewController, AppointmentTypesArr;
@synthesize SelectedBranch, lastIndexPath;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    BpAppointmentTypesViewController *gBpAppointmentTypesViewController=[[BpAppointmentTypesViewController alloc] initWithNibName:@"BpAppointmentTypesViewController" bundle:nil];
    /**/UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom]; 
    [leftButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];    
    leftButton.frame = CGRectMake(0, 0, 50, 33);
    [leftButton addTarget:self action:@selector(handleBack:) forControlEvents:UIControlEventTouchUpInside];
    
    /**/UIButton *RightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [RightButton setImage:[UIImage imageNamed:@"btn_info.png"] forState:UIControlStateNormal];
    RightButton.frame = CGRectMake(0, 0, 62, 33);
    [RightButton addTarget:self action:@selector(handleDesc:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView: leftButton] autorelease];
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView: RightButton] autorelease];
    
    gBpAppointmentTypesViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    [leftButton release];
    [gBpAppointmentTypesViewController release];
    
    
    tblApptypes.layer.cornerRadius=10;
    tblApptypes.layer.borderColor = [UIColor grayColor].CGColor;
    tblApptypes.layer.borderWidth = 1;
    
    [self SelectedBranchArrType];
    
    
    [tblApptypes reloadData];
    
    //show descrtion alert
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Do you want to view the description of the type of appointment we provide?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    
    [alert show];
    
}

-(void)SelectedBranchArrType
{
    if ([self.SelectedBranch isEqualToString:@"Ipoh"])
    {
        
        self.AppointmentTypesArr = [[NSArray alloc] initWithObjects:@"Head2Toe Package", @"Family Program", @"Hearing Assessment", @"Total Wellness Program", @"Treadmill ECG", @"Ultrasound Procedures", @"Echocardiogram", nil];
        
    }else if ([self.SelectedBranch isEqualToString:@"Glenmarie"])
    {
        
        self.AppointmentTypesArr = [[NSArray alloc] initWithObjects:@"Head2Toe Package", @"Family Program", @"Hearing Assessment", @"Total Wellness Program", @"Treadmill ECG", @"Ultrasound Procedures", @"Mammogram", @"CT Scan", @"ENT Services", nil];
        
    }else if ([self.SelectedBranch isEqualToString:@"Klang"])
    {

        self.AppointmentTypesArr = [[NSArray alloc] initWithObjects:@"Head2Toe Package", @"Family Program", @"Hearing Assessment", @"Total Wellness Program", @"Treadmill ECG", @"Ultrasound Procedures", @"Mammogram", @"CT Scan", @"Gastroscopy", @"ENT Services", nil];
    }else
    {
        
        self.AppointmentTypesArr = [[NSArray alloc] initWithObjects:@"Head2Toe Package", @"Family Program", @"Hearing Assessment", @"Total Wellness Program", @"Treadmill ECG", @"Ultrasound Procedures", nil];
        
    }
    //NSLog(@"SelectedBranch>>>>>%@", self.SelectedBranch);
}

- (void)viewDidUnload
{
    [self setTblApptypes:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void) handleBack:(id)sender
{
    // do your custom handler code here
    
    // make sure you do this!
    // pop the controller
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) handleDesc:(id)sender
{
    BpAppointmentTypeDescViewController *gBpAppointmentTypeDescViewController=[[BpAppointmentTypeDescViewController alloc] initWithNibName:@"BpAppointmentTypeDescViewController" bundle:nil];
    
    gBpAppointmentTypeDescViewController.title=@"Description";
    //gBpAppointmentTypesViewController.selectedState=lblSelectedState.text;
    UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:gBpAppointmentTypeDescViewController];
    [self presentModalViewController:navBar animated:YES];
    //        [self.navigationController pushViewController:gBpAppointmentTypeDescViewController animated:YES];
    [gBpAppointmentTypeDescViewController release];
    [navBar release];

}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.AppointmentTypesArr.count;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Configure the cell.
    NSString * sushiName = [self.AppointmentTypesArr objectAtIndex:indexPath.row];
    //NSString *sushiString = [[NSString alloc] initWithFormat:@"%d: %@", indexPath.row, sushiName];
    NSString *sushiString = [[NSString alloc] initWithFormat:@" %@", sushiName];
    cell.textLabel.textColor = [UIColor colorWithRed:(60/255.0) green:(60/255.0) blue:(60/255.0) alpha:1.0];
    cell.textLabel.text = sushiString;
    [sushiString release];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString * sushiName = [self.AppointmentTypesArr objectAtIndex:indexPath.row];
    NSString * sushiString = [NSString stringWithFormat:@"%@", sushiName];    
    NSString * message = [NSString stringWithFormat:@" %@", sushiString];
    /*UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Selected State" message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [alertView show];
    [alertView release];*/
    
    
    UILabel *lblServiceTypeSelected=(UILabel*)[self.gBpNewAppointmentViewController lblAppointmentType];
    
    lblServiceTypeSelected.text=sushiString;
    
    UITableViewCell *newCell = [tableView cellForRowAtIndexPath:indexPath];
    newCell.accessoryType = UITableViewCellAccessoryCheckmark;
    
    if (!self.lastIndexPath) {
        self.lastIndexPath = indexPath;
    }
    
    if ([self.lastIndexPath row] != [indexPath row])
    {
        UITableViewCell *newCell = [tableView cellForRowAtIndexPath: indexPath];
        newCell.accessoryType = UITableViewCellAccessoryCheckmark;
        
        UITableViewCell *oldCell = [tableView cellForRowAtIndexPath:self.lastIndexPath];
        oldCell.accessoryType = UITableViewCellAccessoryNone;
        
        self.lastIndexPath = indexPath;
    }
    else {
        UITableViewCell *newCell = [tableView cellForRowAtIndexPath: indexPath];
        newCell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    //    [self.navigationController popViewControllerAnimated:YES];
}

#pragma alert view delegate method
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"OK"])
    {
        BpAppointmentTypeDescViewController *gBpAppointmentTypeDescViewController=[[BpAppointmentTypeDescViewController alloc] initWithNibName:@"BpAppointmentTypeDescViewController" bundle:nil];
        
        gBpAppointmentTypeDescViewController.title=@"Description";
        //gBpAppointmentTypesViewController.selectedState=lblSelectedState.text;
         UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:gBpAppointmentTypeDescViewController];
        [self presentModalViewController:navBar animated:YES];
//        [self.navigationController pushViewController:gBpAppointmentTypeDescViewController animated:YES];
        [gBpAppointmentTypeDescViewController release];
        [navBar release];
   
    }
    else if([title isEqualToString:@"Cancel"])
    {
//        [self.navigationController popViewControllerAnimated:YES];
    }
}


- (void)dealloc 
{
    [SelectedBranch release];
    [AppointmentTypesArr release];
    [gBpNewAppointmentViewController release];
    [tblApptypes release];
    [super dealloc];
}
@end
