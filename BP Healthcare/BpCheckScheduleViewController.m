//
//  BpCheckScheduleViewController.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpCheckScheduleViewController.h"

@interface BpCheckScheduleViewController ()

@end

@implementation BpCheckScheduleViewController
@synthesize tblCheckUp;
@synthesize CheckArr, loadingView;
@synthesize Bgimg, lblnoItem;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    BpCheckScheduleViewController *gBpCheckScheduleViewController=[[BpCheckScheduleViewController alloc] initWithNibName:@"BpCheckScheduleViewController" bundle:nil];
    /**/UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom]; 
    [leftButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];    
    leftButton.frame = CGRectMake(0, 0, 50, 33);
    [leftButton addTarget:self action:@selector(handleBack:) forControlEvents:UIControlEventTouchUpInside];   
    
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView: leftButton] autorelease]; 
    gBpCheckScheduleViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    [leftButton release];
    [gBpCheckScheduleViewController release];
    
    [self getCheckUp];
    
    lblnoItem.textColor = [UIColor colorWithRed:(160/255.0) green:(1/255.0) blue:(4/255.0) alpha:1.0];
    
    UIView *selfView=self.view;
    LoadingView *temploadingView =[LoadingView loadingViewInView:selfView];
    self.loadingView=temploadingView;
    
    //self.tblCheckUp.rowHeight = 130;
}

- (void)viewDidUnload
{
    [self setTblCheckUp:nil];
    [self setBgimg:nil];
    [self setLblnoItem:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void) handleBack:(id)sender
{
    // do your custom handler code here
    
    // make sure you do this!
    // pop the controller
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)getCheckUp
{
    NetworkHandler *networkHandler=[[NetworkHandler alloc] init];
    BpCheckUpRequest *requestset= [[BpCheckUpRequest alloc] init];
    
    [networkHandler setDelegate:self];
    [networkHandler request:requestset];
    [requestset release];
    [networkHandler release];
}
-(void)handleRecievedResponseMessage:(XMLResponse*)responseMessage
{
    
    if([responseMessage isKindOfClass:[BpCheckUpResponse class]])
    {
        
        //NSString *strMessage=[NSString stringWithFormat:@"%@",((BpCheckUpResponse*)responseMessage).CheckUpArr];
        
        NSArray *msgArr=[[[NSArray alloc]init]autorelease];
        msgArr=((BpCheckUpResponse*)responseMessage).CheckUpArr;
        self.CheckArr=msgArr;
        //resultCount,checkupID,checkupTitle,checkupDescp,checkupSort,checkupStatus,checkupDateTime,checkupImgPath;
        for (BpCheckUp *item in msgArr) {
            //birthitem.resultCount;
            
            /*
            NSLog(@"birthitem.resultCount--->%@",item.resultCount);
            NSLog(@"birthitem.checkupID--->%@",item.checkupID);
            NSLog(@"birthitem.checkupTitle--->%@",item.checkupTitle);
            
            NSLog(@"birthitem.checkupDescp--->%@",item.checkupDescp);
            NSLog(@"birthitem.checkupSort--->%@",item.checkupSort);
            NSLog(@"birthitem.checkupStatus--->%@",item.checkupStatus);
            
            NSLog(@"birthitem.checkupDateTime--->%@",item.checkupDateTime);
            NSLog(@"birthitem.checkupImgPath--->%@",item.checkupImgPath);
             */
            
            if (![item.resultCount isEqualToString:@"0"])
            {
                
                Bgimg.hidden=YES;
                lblnoItem.hidden=YES;
                
            }else
            {
                Bgimg.hidden=NO;
                lblnoItem.hidden=NO;
            }
            
        }
        
        [tblCheckUp reloadData];
        [self.loadingView removeView];
    }else {
        /**/UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Warning!" message:@"Server down. Please try again later. Thanks you." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertView show];
        [alertView release];
        
        [self.loadingView removeView];
    } 
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.CheckArr count];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath: (NSIndexPath *) indexPath {
    cell = (BpCheckUpCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.frame.size.height;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIDentifier=@"Cell";
    
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIDentifier];
    cell= (BpCheckUpCell*)[tableView dequeueReusableCellWithIdentifier:cellIDentifier];
    if (cell == nil) {
        cell = [[[BpCheckUpCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIDentifier]autorelease];
    }
    BpCheckUp* classBpCheckUp=[self.CheckArr objectAtIndex:indexPath.row];
    
    cell.BpCheckUpcell=classBpCheckUp;
    
    cell.imgsmallflag.image=[UIImage imageNamed:@"icon_schedule_2.png"];
    
    //(120, 60, 220, 50)
    /*=============*/
    
    CGSize maxsizetitle = CGSizeMake(220, 9999);
    UIFont *thefonttitle = [UIFont fontWithName:@"Helvetica-Bold" size:17];
    CGSize textsizetitle = [classBpCheckUp.checkupTitle sizeWithFont:thefonttitle constrainedToSize:maxsizetitle lineBreakMode:UILineBreakModeWordWrap];
    
    cell.lblTitle.numberOfLines=20;
    cell.lblTitle.frame=CGRectMake(80, 10, 220, textsizetitle.height);
    cell.lblTitle.lineBreakMode=UILineBreakModeWordWrap;
    cell.lblTitle.text=classBpCheckUp.checkupTitle;
    
    CGSize maxsize = CGSizeMake(220, 9999);
    UIFont *thefont = [UIFont fontWithName:@"Helvetica" size:12];
    CGSize textsize = [classBpCheckUp.checkupDescp sizeWithFont:thefont constrainedToSize:maxsize lineBreakMode:UILineBreakModeWordWrap];
    
    cell.lblRemark.frame=CGRectMake(15, textsizetitle.height+20, 100, 21);
    
    cell.lblRemarktext.numberOfLines=20;
    cell.lblRemarktext.frame=CGRectMake(80, textsizetitle.height+23, 220, textsize.height);
    cell.lblRemarktext.lineBreakMode = UILineBreakModeWordWrap;
    cell.lblRemarktext.text=classBpCheckUp.checkupDescp;
    
    cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, 320, textsizetitle.height+textsize.height+30);
    
    /*=============*/
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    //[self.loadingView removeView];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    /*if(![self NetworkStatus])
     {
     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"You're not connected to Internet" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
     [alert show];
     [alert release];
     }
     else
     {
     
     
     
     BpPromotionCell *selectedcell= (BpPromotionCell*)[tableView cellForRowAtIndexPath:indexPath];    
     //NSLog(@"%d",selectedcell.CAgendaEvent.AgendaEventID );
     
     
     AgendaEventDetailView *gAgendaEventDetailView=[[AgendaEventDetailView alloc] initWithNibName:@"AgendaEventDetailView" bundle:nil];
     
     NSString *sellectedAgendaID=[[NSString alloc]initWithFormat:@"%d",selectedcell.CAgendaEvent.AgendaEventID];
     gAgendaEventDetailView.gAgendaDetainID=sellectedAgendaID;
     gAgendaEventDetailView.agendaEvent=selectedcell.CAgendaEvent;
     gAgendaEventDetailView.title=@"Tupperware Brands Asia Pasific";
     
     [self.navigationController pushViewController:gAgendaEventDetailView animated:YES];*/
    
    /*BulletinBoardDetail *gBulletinBoardDetail=[[BulletinBoardDetail alloc] initWithNibName:@"BulletinBoardDetail" bundle:nil];
     gBulletinBoardDetail.BulletinID= selectedcell.bulletinMessage.ID;
     gBulletinBoardDetail.Bulletintitle=selectedcell.bulletinMessage.Title;
     gBulletinBoardDetail.Bulletintimedate=selectedcell.bulletinMessage.Time_Date;
     gBulletinBoardDetail.Bulletinbody=selectedcell.bulletinMessage.Body;
     gBulletinBoardDetail.Bulletinimagepath=selectedcell.bulletinMessage.ImagePath;
     
     
     gBulletinBoardDetail.title=@"Tupperware Brands Asia Pasific";
     
     [self.navigationController pushViewController:gBulletinBoardDetail animated:YES];*/
    //}
}




- (void)dealloc 
{
    [loadingView release];
    [CheckArr release];
    [tblCheckUp release];
    [Bgimg release];
    [lblnoItem release];
    [super dealloc];
}
@end





