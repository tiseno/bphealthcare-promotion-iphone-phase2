//
//  BpCustomerNameViewController.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 9/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
@class BpNewAppointmentViewController;

@interface BpCustomerNameViewController : UIViewController{
    
}
@property (nonatomic, retain) BpNewAppointmentViewController *gBpNewAppointmentViewController;
-(IBAction)DoneNametapped:(id)sender;
@property (retain, nonatomic) IBOutlet UITextView *txtName;

@end
