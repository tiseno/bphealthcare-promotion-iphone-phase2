//
//  BpEvent.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpEvent.h"

@implementation BpEvent
@synthesize resultCount, eventsID, eventsSort, eventsDescp, eventsTitle, eventsStatus, eventsImgPath, eventsDateTime, eventsCreateDate;

-(void)dealloc
{
    [eventsCreateDate release];
    [eventsDateTime release];
    [eventsDescp release];
    [eventsID release];
    [eventsImgPath release];
    [eventsSort release];
    [eventsTitle release];
    [eventsStatus release];
    [resultCount release];
    [super dealloc];
}
@end
