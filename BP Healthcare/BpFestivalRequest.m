//
//  BpFestivalRequest.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpFestivalRequest.h"

@implementation BpFestivalRequest

-(id)init
{
    self=[super init];
    if(self)
    {
        self.SOAPAction=@"retrieveFestival";
        self.requestType=WebserviceRequest;
    }
    return self;
}
-(NSString*) generateHTTPPostMessage
{
    return @"";
}

@end
