//
//  BpGetAllItemsResponse.h
//  BP Healthcare
//
//  Created by desmond on 13-1-7.
//
//

#import <Foundation/Foundation.h>
#import "XMLResponse.h"

@interface BpGetAllItemsResponse : XMLResponse{

}

@property (nonatomic, retain) NSArray *PromotionsArr;

@end
