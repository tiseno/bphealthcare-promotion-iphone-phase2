//
//  BpGetAllTransactionResponse.h
//  BP Healthcare
//
//  Created by desmond on 13-2-7.
//
//

#import "XMLResponse.h"

@interface BpGetAllTransactionResponse : XMLResponse

@property (nonatomic, retain) NSArray *TransactionArr;

@end
