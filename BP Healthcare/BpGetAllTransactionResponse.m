//
//  BpGetAllTransactionResponse.m
//  BP Healthcare
//
//  Created by desmond on 13-2-7.
//
//

#import "BpGetAllTransactionResponse.h"

@implementation BpGetAllTransactionResponse
@synthesize TransactionArr;

-(void)dealloc
{
    [TransactionArr release];
    [super dealloc];
}
@end
