//
//  BpGetAllTransactionsRequest.h
//  BP Healthcare
//
//  Created by desmond on 13-2-7.
//
//

#import "XMLRequest.h"
#import "BpAppDelegate.h"

@interface BpGetAllTransactionsRequest : XMLRequest

@end
