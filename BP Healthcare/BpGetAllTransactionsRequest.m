//
//  BpGetAllTransactionsRequest.m
//  BP Healthcare
//
//  Created by desmond on 13-2-7.
//
//

#import "BpGetAllTransactionsRequest.h"

@implementation BpGetAllTransactionsRequest

-(id)init
{
    self=[super init];
    if(self)
    {
        self.SOAPAction=@"GetOrderByTokenDevice";
        self.requestType=WebserviceRequest;
    }
    return self;
}

-(NSString*) generateHTTPPostMessage
{
    BpAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    NSString *token = appDelegate.TokenString;
    
    NSString *postMsg = [NSString stringWithFormat:
                         @"TokenDevice=%@",token];
    //    NSLog(@"postMsg-----> %@",postMsg);
    return postMsg;
}
@end
