//
//  BpHealthTipsViewController.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpHealthTipsViewController.h"

@interface BpHealthTipsViewController ()

@end

@implementation BpHealthTipsViewController
@synthesize healthArr;
@synthesize tblhealthtips, loadingView;
@synthesize Bgimg, lblnoItem;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    BpHealthTipsViewController *gBpHealthTipsViewController=[[BpHealthTipsViewController alloc] initWithNibName:@"BpHealthTipsViewController" bundle:nil];
    /**/UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom]; 
    [leftButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];    
    leftButton.frame = CGRectMake(0, 0, 50, 33);
    [leftButton addTarget:self action:@selector(handleBack:) forControlEvents:UIControlEventTouchUpInside];   
    
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView: leftButton] autorelease]; 
    gBpHealthTipsViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    [leftButton release];
    [gBpHealthTipsViewController release];
    
    [self getHealthTips];
    lblnoItem.textColor = [UIColor colorWithRed:(160/255.0) green:(1/255.0) blue:(4/255.0) alpha:1.0];
    
    self.tblhealthtips.layer.borderColor = [UIColor colorWithRed:(170/255.0) green:(170/255.0) blue:(170/255.0) alpha:1.0].CGColor;
    UIView *selfView=self.view;
    LoadingView *temploadingView =[LoadingView loadingViewInView:selfView];
    self.loadingView=temploadingView;
    //self.tblhealthtips.rowHeight = 130;
} 

- (void)viewDidUnload
{
    [self setTblhealthtips:nil];
    [self setBgimg:nil];
    [self setLblnoItem:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void) handleBack:(id)sender
{
    // do your custom handler code here
    
    // make sure you do this!
    // pop the controller
    for(UIView* uiview in self.view.subviews)
    {
        [uiview removeFromSuperview];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)getHealthTips
{
    NetworkHandler *networkHandler=[[NetworkHandler alloc] init];
    BpHealthTipsRequest *requestset= [[BpHealthTipsRequest alloc] init];
    
    [networkHandler setDelegate:self];
    [networkHandler request:requestset];
    [requestset release];
    [networkHandler release];
}

-(void)handleRecievedResponseMessage:(XMLResponse*)responseMessage
{
    
    if([responseMessage isKindOfClass:[BpHealthTipsResponse class]])
    {
        
        //NSString *strMessage=[NSString stringWithFormat:@"%@",((BpCheckUpResponse*)responseMessage).CheckUpArr];
        
        NSArray *msgArr=[[[NSArray alloc]init]autorelease];
        msgArr=((BpHealthTipsResponse*)responseMessage).HealthTipsArr;
        self.healthArr=msgArr;
        
        for (BpHealthTips *item in msgArr) {

            /*
            NSLog(@"birthitem.resultCount--->%@",item.resultCount);
            NSLog(@"birthitem.healthtipsImgPath--->%@",item.healthtipsImgPath);
            NSLog(@"birthitem.healthtipsSort--->%@",item.healthtipsSort);
            
            NSLog(@"birthitem.healthtipsStatus--->%@",item.healthtipsStatus);
            NSLog(@"birthitem.healthtipsTitle--->%@",item.healthtipsTitle);
            NSLog(@"birthitem.healthtipsID--->%@",item.healthtipsID);
            
            NSLog(@"birthitem.healthtipsDescp--->%@",item.healthtipsDescp);
            NSLog(@"birthitem.healthtipsDateTime--->%@",item.healthtipsDateTime);

            */
            
            if (![item.resultCount isEqualToString:@"0"])
            {
                
                Bgimg.hidden=YES;
                lblnoItem.hidden=YES;
                
            }else
            {
                Bgimg.hidden=NO;
                lblnoItem.hidden=NO;
            }
        }
        [tblhealthtips reloadData];
        [self.loadingView removeView];
    }else {
        /**/UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Warning!" message:@"Server down. Please try again later. Thanks you." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertView show];
        [alertView release];
        
        [self.loadingView removeView];
    } 
    
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.healthArr count];
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIDentifier=@"Cell";
    
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIDentifier];
    cell= (BpHealthyTipsCell*)[tableView dequeueReusableCellWithIdentifier:cellIDentifier];
    if (cell == nil) {
        cell = [[[BpHealthyTipsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIDentifier]autorelease];
        
        
        
    }
    BpHealthTips* classBpHealthTips=[self.healthArr objectAtIndex:indexPath.row];
    
    cell.gBpHealthTips=classBpHealthTips;
    
    cell.imgsmallflag.image=[UIImage imageNamed:@"icon_tips_2.png"];

    
    if (classBpHealthTips.healthtipsImgPath.length!=0) 
    {

        
        
        /*=============*/
        CGSize maxsizetitle = CGSizeMake(220, 9999);
        UIFont *thefonttitle = [UIFont fontWithName:@"Helvetica-Bold" size:17];
        CGSize textsizetitle = [classBpHealthTips.healthtipsTitle sizeWithFont:thefonttitle constrainedToSize:maxsizetitle lineBreakMode:UILineBreakModeWordWrap];
        
        cell.lblTitle.numberOfLines=20;
        cell.lblTitle.frame=CGRectMake(80, 10, 220, textsizetitle.height);
        cell.lblTitle.lineBreakMode=UILineBreakModeWordWrap;
        cell.lblTitle.text=classBpHealthTips.healthtipsTitle;
        
        
        CGSize maxsize = CGSizeMake(220, 9999);
        UIFont *thefont = [UIFont fontWithName:@"Helvetica" size:12];
        CGSize textsize = [classBpHealthTips.healthtipsDescp sizeWithFont:thefont constrainedToSize:maxsize lineBreakMode:UILineBreakModeWordWrap];
        
        cell.lblBodytext.numberOfLines=120;
        cell.lblBodytext.frame=CGRectMake(80, textsizetitle.height+20, 220, textsize.height);
        cell.lblBodytext.lineBreakMode=UILineBreakModeWordWrap;
        cell.lblBodytext.text=classBpHealthTips.healthtipsDescp;
        
        //cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, 320, textsizetitle.height+textsize.height+50);
        
        if ((textsizetitle.height+textsize.height+30)<130) {
            
            cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, 320, 150);
        }else
        {
            cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, 320, textsizetitle.height+textsize.height+30);
        }
        
        NSString *itemimg=[classBpHealthTips.healthtipsImgPath stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        NSString *imagename=[[NSString alloc]initWithFormat:@"http://203.106.245.234:100/images/healthtips/%@",itemimg];
        cell.url=imagename;
        cell.imgpicture.imageURL = [NSURL URLWithString:imagename];
                
    }else 
    {
        /*=============*/
        CGSize maxsize = CGSizeMake(220, 9999);
        UIFont *thefont = [UIFont fontWithName:@"Helvetica" size:12];
        CGSize textsize = [classBpHealthTips.healthtipsDescp sizeWithFont:thefont constrainedToSize:maxsize lineBreakMode:UILineBreakModeWordWrap];
        
        cell.lblBodyNoPictext.numberOfLines=20;
        cell.lblBodyNoPictext.frame=CGRectMake(10, 35, 220, textsize.height+50);
        cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, 320, textsize.height+100);
        
        /*=============*/
        
        cell.lblBodyNoPictext.text=classBpHealthTips.healthtipsDescp;
    }
    
    //cell.imageView.frame = CGRectMake(0.0f, 0.0f, 44.0f, 44.0f);
    //float width = 40;
    
    
    

    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    //[self.loadingView removeView];
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath: (NSIndexPath *) indexPath
{
    cell = (BpHealthyTipsCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.frame.size.height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    BpHealthyTipsCell *selectedcell= (BpHealthyTipsCell*)[tableView cellForRowAtIndexPath:indexPath]; 

    
    //BpPromotions* classBpPromotions=[self.promtArr objectAtIndex:indexPath.row];
    
    //cell.BpPromotionscell=classBpPromotions;
    NSLog(@"selectedcell.gBpHealthTips.healthtipsImgPath>>>>>%@",selectedcell.gBpHealthTips.healthtipsImgPath);
    
    NSString *itemDescription=[selectedcell.gBpHealthTips.healthtipsImgPath stringByReplacingOccurrencesOfString:@" " withString:@"%20"]; 
    
    if (![selectedcell.gBpHealthTips.healthtipsImgPath isEqualToString:@"none"]) {
        NSString *imagename=[[NSString alloc]initWithFormat:@"http://203.106.245.234:100/images/healthtips/a%@",itemDescription];
        
        
        BpPromotionImageViewController *gBpPromotionImageViewController=[[BpPromotionImageViewController alloc] initWithNibName:@"BpPromotionImageViewController" bundle:nil];
        gBpPromotionImageViewController.title=@"Health Tips";
        gBpPromotionImageViewController.imgPath=imagename;
        [self.navigationController pushViewController:gBpPromotionImageViewController animated:YES];
        [gBpPromotionImageViewController release];
    }
    
    
    /*
    [MTPopupWindow showWindowWithHTMLFile:imagename insideView:self.view];
    
    UIView *selfView=self.view;
    LoadingView *temploadingView =[LoadingView loadingViewInView:selfView];
    self.loadingView=temploadingView;
    
    [self performSelector:@selector(showloadingView) withObject:nil afterDelay:2];*/
}

-(void)showloadingView
{
    [self.loadingView removeView];
}

-(void)dealloc
{
    [loadingView release];
    [healthArr release];
    [tblhealthtips release];
    [Bgimg release];
    [lblnoItem release];
    [super dealloc];
}

@end
