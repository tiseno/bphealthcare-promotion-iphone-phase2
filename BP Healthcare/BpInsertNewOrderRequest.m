//
//  BpInsertNewOrderRequest.m
//  BP Healthcare
//
//  Created by desmond on 13-1-10.
//
//

#import "BpInsertNewOrderRequest.h"

@implementation BpInsertNewOrderRequest
@synthesize tokendevice,
            name,
            email,
            contact,
            fax,
            billadd1,
            billadd2,
            billtown,
            billpostcode,
            shipadd1,
            shipadd2,
            shiptown,
            shippostcode,
            remark,
            currency,
            amount,
            itemName,
            deliverymethod,
            pickupbranch,
            pickupstate;

-(id)init
{
    self=[super init];
    if(self)
    {
        self.SOAPAction=@"InsertNewOrder";
        self.requestType=WebserviceRequest;
    }
    return self;
}

-(NSString*) generateHTTPPostMessage
{
    //userID=string&serviceReq=string&appDate=string&customerName=string&customerContact=string&customerEmail=string&appBranch=string&noOfPerson=string&custRemark=string&tokenID=string
//   tokendevice = @"1234sdf";
    NSLog(@"token: %@",tokendevice);
    NSString *postMsg = [NSString stringWithFormat:
                         @"tokendevice=%@"
                         "&name=%@"
                         "&email=%@"
                         "&contact=%@"
                         "&fax=%@"
                         "&billadd1=%@"
                         "&billadd2=%@"
                         "&billtown=%@"
                         "&billpostcode=%@"
                         "&shipadd1=%@"
                         "&shipadd2=%@"
                         "&shiptown=%@"
                         "&shippostcode=%@"
                         "&remark=%@"
                         "&currency=%@"
                         "&amount=%@"
                         "&itemName=%@"
                         "&deliverymethod=%@"
                         "&pickupbranch=%@"
                         "&pickupstate=%@",
                         tokendevice,name,
                         email,contact,
                         fax,billadd1,
                         billadd2,billtown,
                         billpostcode,shipadd1,
                         shipadd2,shiptown,
                         shippostcode,remark,
                         currency,amount,
                         itemName,deliverymethod,
                         pickupbranch,pickupstate];
    
    return postMsg;
}

@end
