//
//  BpInsertNewOrderResponse.m
//  BP Healthcare
//
//  Created by desmond on 13-1-18.
//
//

#import "BpInsertNewOrderResponse.h"

@implementation BpInsertNewOrderResponse
@synthesize ReferenceArr;

-(void)dealloc
{
    [ReferenceArr release];
    [super dealloc];
}


@end
