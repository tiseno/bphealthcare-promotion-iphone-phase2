//
//  BpIpay88ViewController.m
//  BP Healthcare
//
//  Created by desmond on 13-1-15.
//
//

#import "BpIpay88ViewController.h"
#define personalDetailFileName @"personalDetail.plist"

@interface BpIpay88ViewController ()
@property(retain, nonatomic) NSMutableDictionary *paymentResult;
@end

@implementation BpIpay88ViewController
@synthesize paymentsdk, resultDelegate;
@synthesize resultView,paymentView,paymentResult,LabelCustomerName,LabelReferenceID,resultViewFailed;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    BpIpay88ViewController *gBpLatestPromotionViewController=[[BpIpay88ViewController alloc] initWithNibName:@"BpIpay88ViewController" bundle:nil];
    
    /**/UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton setImage:[UIImage imageNamed:@"btn_close.png"] forState:UIControlStateNormal];
    leftButton.frame = CGRectMake(0, 0, 62, 33);
    [leftButton addTarget:self action:@selector(handleBack:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView: leftButton] autorelease];
    gBpLatestPromotionViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];

    if ([self.navigationController.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)] ) {
        UIImage *image = [UIImage imageNamed:@"bar_top.png"];
        [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    }
    
    self.navigationItem.title = @"Payment";
    [leftButton release];
    [gBpLatestPromotionViewController release];
    
    //get user info
    NSString *filePath = [self dataFilePath];
    if([[NSFileManager defaultManager]  fileExistsAtPath:filePath]){
        NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:filePath];
        _userName = [dict objectForKey:@"name"];
        _userEmail = [dict objectForKey:@"email"];
        _userContact = [dict objectForKey:@"contact"];
        _amount = [dict objectForKey:@"lastBillingAmount"];
        _prodName = [dict objectForKey:@"lastBuyingProduct"];
        _prodQuantity = [dict objectForKey:@"productQuantity"];
        NSLog(@"NSFileManager");
    }
    
    paymentsdk = [[Ipay alloc] init];
    paymentsdk.delegate = self;
    IpayPayment *payment = [[IpayPayment alloc] init];
    [payment setPaymentId:@""];
    [payment setMerchantKey:@"jbML8ZOHuC"];
    [payment setMerchantCode:@"M05457"];
//    [payment setMerchantKey:@"JXqVd2QZu7"];
//    [payment setMerchantCode:@"M04204"];
    [payment setRefNo:_referenceNumber];
    [payment setAmount:_amount];
    [payment setCurrency:@"MYR"];
    [payment setProdDesc:[[NSString alloc ]initWithFormat:@"%@ X %@ " ,_prodName,_prodQuantity]];
    [payment setUserName:_userName];
    [payment setUserEmail:_userEmail];
    [payment setUserContact:_userContact];
    [payment setRemark:[[NSString alloc] initWithFormat:@"Username: %@  Total Amount: %@",_userName,_amount]];
    [payment setLang:@"ISO-8859-1"];
    
    NSLog(@"NSFileManager: %@",_userEmail);

     paymentView = [paymentsdk checkout:payment];
    [self.view addSubview:paymentView];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSString *)dataFilePath{
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [path objectAtIndex:0];
    
    return [documentsDirectory stringByAppendingPathComponent:personalDetailFileName];
}

- (void)dealloc {
    [resultView release];

    [_amount release];
    [_userName release];
    [_userEmail release];
    [_userContact release];
    [_remark release];
    [_prodDesc release];
    [resultViewFailed release];
    [LabelCustomerName release];
    [LabelReferenceID release];
     [super dealloc];
}
- (void)viewDidUnload {
    [self setResultView:nil];
    [self setResultViewFailed:nil];
    [self setLabelCustomerName:nil];
    [self setLabelReferenceID:nil];
    [super viewDidUnload];
    [_amount release];
    [_userName release];
    [_userEmail release];
    [_userContact release];
    [_remark release];
    [_prodDesc release];
}

- (void) handleBack:(id)sender
{
    if(paymentResult == nil)
        [self paymentCancelled:_referenceNumber withTransId:@"" withAmount:_amount withRemark:@"Customer Cancel Payment" withErrDesc:@"Customer Cancel Payment" ];
    else
        [self dismissModalViewControllerAnimated:YES];
}


#pragma Ipay88 delegate method
- (void)paymentSuccess:(NSString *)refNo withTransId:(NSString *)transId withAmount:(NSString *)amount withRemark:(NSString *)remark withAuthCode:(NSString *)authCode{
    NSLog(@"paymentSuccess:  refNo:%@ transId:%@ amount:%@ remark:%@ authCode:%@",
          refNo,transId,amount,remark,authCode);

    [paymentView removeFromSuperview];
    
    LabelCustomerName.text = [[NSString alloc] initWithFormat:@"Customer Name : %@", _userName];
    LabelReferenceID.text = [[NSString alloc] initWithFormat:@"Reference ID : %@", refNo];
    [self.view addSubview:resultView];
    
    paymentResult = [[NSDictionary alloc] initWithObjectsAndKeys:
                                   refNo,@"referenceNo",
                                   @"Successful",@"paymentStatus",
                                   transId,@"transactionid",
                                   amount,@"amount",
                                   remark,@"remark",
                                   authCode,@"authcode",nil];
    [resultDelegate paymentResult:paymentResult];    
}

- (void)paymentFailed:(NSString *)refNo withTransId:(NSString *)transId withAmount:(NSString *)amount withRemark:(NSString *)remark withErrDesc:(NSString *)errDesc{
    
    NSLog(@"paymentSuccess:  refNo:%@ transId:%@ amount:%@ remark:%@ errDesc:%@",
          refNo,transId,amount,remark,errDesc);


    [paymentView removeFromSuperview];
    
    [self.view addSubview:resultViewFailed];
    
    paymentResult = [[NSDictionary alloc] initWithObjectsAndKeys:
                     refNo,@"referenceNo",
                     @"Failed",@"paymentStatus",
                     transId,@"transactionid",
                     amount,@"amount",
                     errDesc,@"remark",
                     @"",@"authcode",nil];
    [resultDelegate paymentResult:paymentResult];

}

- (void)paymentCancelled:(NSString *)refNo withTransId:(NSString *)transId withAmount:(NSString *)amount withRemark:(NSString *)remark withErrDesc:(NSString *)errDesc{
    
    paymentResult = [[NSDictionary alloc] initWithObjectsAndKeys:
                     refNo,@"referenceNo",
                     @"Not yet proccess",@"paymentStatus",
                     transId,@"transactionid",
                     amount,@"amount",
                     errDesc,@"remark",
                     @"",@"authcode",nil];
    [resultDelegate paymentResult:paymentResult];
        [self dismissModalViewControllerAnimated:YES];
}

- (IBAction)endPayment:(id)sender {
    [self dismissModalViewControllerAnimated:YES];
}
@end
