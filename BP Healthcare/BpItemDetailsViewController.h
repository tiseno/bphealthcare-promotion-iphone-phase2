//
//  BpItemDetailsViewController.h
//  BP Healthcare
//
//  Created by desmond on 13-1-8.
//
//

#import <UIKit/UIKit.h>
#import "BpPromotionItem.h"
#import "AsyncImageView.h"
#import "BpOrderDetailsViewController.h"
#import "BpLatestPromotionViewController.h"
#import "LoadingView.h"

@interface BpItemDetailsViewController : UIViewController
@property (retain, nonatomic) IBOutlet UIImageView *imageView;
@property (retain, nonatomic) UIImage *img;
@property (retain, nonatomic) IBOutlet UILabel *titleLabel;
@property (retain, nonatomic) IBOutlet UILabel *promotionPriceLabel;
@property (retain, nonatomic) IBOutlet UILabel *NPLabel;
@property (retain, nonatomic) IBOutlet UIScrollView *contentScrollView;
@property (retain, nonatomic) IBOutlet UILabel *descptionLabel;
@property (retain, nonatomic) IBOutlet UILabel *quantityLabel;
@property (retain, nonatomic) IBOutlet UIButton *confirmButton;
@property (retain, nonatomic) IBOutlet UIButton *cancelButton;
@property (retain, nonatomic) IBOutlet UIButton *minusButton;
@property (retain, nonatomic) IBOutlet UIButton *addButton;
@property (nonatomic, retain) LoadingView *loadingView;

@property (retain , nonatomic) BpPromotionItem *promotionItem;
@property (retain , nonatomic) BpOrderDetailsViewController *gBpOrderDetailsViewController;
@property NSInteger quantity;

- (IBAction)quantityAdd:(UIButton *)sender;
- (IBAction)quantityMinus:(UIButton *)sender;
- (IBAction)confirmDeal:(UIButton *)sender;
- (IBAction)cancelDeal:(UIButton *)sender;

@end
