//
//  BpLatestPromotionViewController.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NetworkHandler.h"
#import "Reachability.h"
#import "BpPromotions.h"
#import "BpPromotionsRequest.h"
#import "BpPromotionsResponse.h"
#import "BpPromotionCell.h"
#import "BpPromotionItemCell.h"
#import "BpItemDetailsViewController.h"
#import "BpGetAllItem.h"
#import "LoadingView.h"
#import "AsyncImageView.h"
#import "MTPopupWindow.h"
#import "BpPromotionImageViewController.h"
#import "BpTransactionViewController.h"
#import <CoreLocation/CoreLocation.h>
#import <QuartzCore/CALayer.h>

@interface BpLatestPromotionViewController : UIViewController<NetworkHandlerDelegate,UITableViewDelegate, UITableViewDataSource>{
    BpPromotionCell *cell;
    int indeximg;
}

-(void)getPromotions;
@property (retain, nonatomic) IBOutlet UITableView *tblPromotion;
@property (nonatomic, retain) NSArray *promtArr;
@property (nonatomic, retain) LoadingView *loadingView;
@property (nonatomic, retain) NSString *enlargeImagePath;
@property (retain, nonatomic) IBOutlet UIImageView *Bgimg;
@property (retain, nonatomic) UIImage *defaultImage;
@property (retain, nonatomic) IBOutlet UILabel *lblnoItem;

@end
