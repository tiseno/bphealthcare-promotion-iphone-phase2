//
//  BpLatestPromotionViewController.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpLatestPromotionViewController.h"

@interface BpLatestPromotionViewController ()

@end

@implementation BpLatestPromotionViewController
@synthesize tblPromotion;
@synthesize promtArr, loadingView;
@synthesize enlargeImagePath;
@synthesize Bgimg, lblnoItem;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    BpLatestPromotionViewController *gBpLatestPromotionViewController=[[BpLatestPromotionViewController alloc] initWithNibName:@"BpLatestPromotionViewController" bundle:nil];
    /**/UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];
    leftButton.frame = CGRectMake(0, 0, 50, 33);
    [leftButton addTarget:self action:@selector(handleBack:) forControlEvents:UIControlEventTouchUpInside];
    
    /**/UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setImage:[UIImage imageNamed:@"btn_history.png"] forState:UIControlStateNormal];
    rightButton.frame = CGRectMake(0, 0, 62, 33);
    [rightButton addTarget:self action:@selector(handleHistory:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView: leftButton] autorelease]; 
    gBpLatestPromotionViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView: rightButton] autorelease];
    gBpLatestPromotionViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];

    [rightButton release];
    [leftButton release];
    [gBpLatestPromotionViewController release];
    
    _defaultImage = [UIImage imageNamed:@"icon_promotions_2.png"];
    [self getPromotions]; 
    lblnoItem.textColor = [UIColor colorWithRed:(160/255.0) green:(1/255.0) blue:(4/255.0) alpha:1.0];
    
    UIView *selfView=self.view;
    LoadingView *temploadingView =[LoadingView loadingViewInView:selfView];
    self.loadingView=temploadingView;
    
}

- (void)viewDidUnload
{
    [self setTblPromotion:nil];
    [self setBgimg:nil];
    [self setLblnoItem:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void) handleBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) handleHistory:(id)sender
{
    BpTransactionViewController *gBpTransactionViewController = [[BpTransactionViewController alloc] initWithNibName:@"BpTransactionViewController" bundle:nil];
    [self.navigationController pushViewController:gBpTransactionViewController animated:YES];
    [gBpTransactionViewController release];
}

-(void)getPromotions
{
    NetworkHandler *networkHandler2=[[NetworkHandler alloc] init];
    networkHandler2.webserviceURLType = @"service.asmx";
    BpGetAllItem *requestset2= [[BpGetAllItem alloc] init];
    [networkHandler2 setDelegate:self];
    [networkHandler2 request:requestset2];
    
}

-(void)handleRecievedResponseMessage:(XMLResponse*)responseMessage
{
    
    if([responseMessage isKindOfClass:[BpGetAllItemsResponse class]])
    {
        NSArray *msgArr=[[[NSArray alloc]init]autorelease];
        msgArr=((BpGetAllItemsResponse *)responseMessage).PromotionsArr;

        self.promtArr=msgArr;
        for (BpPromotionItem *item in msgArr)
         {             
             if ([promtArr count] > 0)
             {
                 
                 Bgimg.hidden=YES;
                 lblnoItem.hidden=YES;
                 
             }else
             {
                 Bgimg.hidden=NO;
                 lblnoItem.hidden=NO;
             }

            
        }
        
        [tblPromotion reloadData];
        [self.loadingView removeView];
        
    }else {
        /**/UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Warning!" message:@"Server down. Please try again later. Thanks you." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertView show];
        [alertView release];
        
        [self.loadingView removeView];
    } 
    
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath: (NSIndexPath *) indexPath {
    cell = (BpPromotionCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.frame.size.height;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.promtArr count];
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    static NSString *cellIDentifier=@"Cell";
    
    static NSString *CellTableIdentifier = @"PromotionCellIdentifier";
    static BOOL nibsResgisted = NO;

    [tableView registerNib:[UINib nibWithNibName:@"BpPromotionItemCell" bundle:nil] forCellReuseIdentifier:CellTableIdentifier];
    BpPromotionItemCell *cell = [tableView dequeueReusableCellWithIdentifier:CellTableIdentifier];
    
    NSUInteger row = [indexPath row];
    BpPromotionItem* classBpPromotions=[self.promtArr objectAtIndex:row];
    
    [cell setData:classBpPromotions.Name UILabel:cell.titleLabel];
    
    NSString *disPrice = [[NSString alloc] initWithFormat:@"RM %@",classBpPromotions.DiscountPrice];
    [cell setData:disPrice  UILabel:cell.promotionPriceLabel];
    
    NSString *NPrice = [[NSString alloc] initWithFormat:@"RM %@",classBpPromotions.NormalPrice];
    [cell setData:NPrice UILabel:cell.NPLabel];
    
    //Strike Through line
    UIView* slabel = [[UIView alloc] initWithFrame:CGRectMake(cell.NPLabel.frame.origin.x, cell.NPLabel.frame.origin.y+10, cell.NPLabel.frame.size.width, 1)];
    CGAffineTransform rotationTransform = CGAffineTransformIdentity;
    rotationTransform = CGAffineTransformRotate(rotationTransform, (-30.0/360.0));
    slabel.transform = rotationTransform;

    [cell addSubview:slabel];
    [slabel setBackgroundColor:[UIColor blackColor]];
    
    [cell setData:classBpPromotions.Descp UILabel:cell.descriptionLabel];
    
    //set image border
    cell.imageView.layer.masksToBounds = YES;
    cell.imageView.layer.borderColor = [UIColor grayColor].CGColor;
    cell.imageView.layer.borderWidth = 1;

    if(classBpPromotions.PicURl != @""){
        NSString *itemimg=[classBpPromotions.PicURl stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        NSString *imagename=[[NSString alloc]initWithFormat:@"http://203.106.245.234:100/images/items/%@",itemimg];
        cell.url=imagename;
        cell.imageView.imageURL = [NSURL URLWithString:imagename];
    }else{
        cell.imageView.image = _defaultImage;
    }    return cell;
}

-(void)ImageTapped
{
    NSLog(@"hello!!!");
    //NSLog(@"self.enlargeImagePath!!!--->%@",self.enlargeImagePath);
    //NSString *largeimage = [imagepath stringByReplacingCharactersInRange:NSMakeRange(imagepath.length - 5, 1) withString:@"M"];
    //[MTPopupWindow showWindowWithHTMLFile:largeimage insideView:self.view];
    
    NSLog(@"indeximg--->%d", indeximg);
    BpPromotions* classBpPromotions=[self.promtArr objectAtIndex:indeximg];
    NSLog(@"classBpPromotions--->%@", classBpPromotions.promotionImgPath);
    
    
    
    /*for (BpPromotions *imgitm in classBpPromotions) {
        
        //imgitm.promotionImgPath;
        //NSLog(@"imgitm.promotionImgPath--->%@", imgitm.promotionImgPath);
    }
    
    for(int i=0;i<classBpPromotions.count;i++)
    {
        
    }*/
    
    //[MTPopupWindow showWindowWithHTMLFile:self.enlargeImagePath insideView:self.view];
    [enlargeImagePath release];
}

-(void)showloadingView
{
    [self.loadingView removeView];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSUInteger row = [indexPath row];
    BpPromotionItem* classBpPromotions=[self.promtArr objectAtIndex:row];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        
    BpItemDetailsViewController *gBpItemViewController = [[BpItemDetailsViewController alloc] initWithNibName:@"BpItemDetailsViewController" bundle:nil];
    gBpItemViewController.promotionItem = classBpPromotions;
     
    if(cell.imageView.image == _defaultImage){
        gBpItemViewController.img = nil;
    }else{
        gBpItemViewController.img = cell.imageView.image;
    }

    
    [self.navigationController pushViewController:gBpItemViewController animated:YES];
    [gBpItemViewController release];
    
    /*[MTPopupWindow showWindowWithHTMLFile:imagename insideView:self.view];
    
    UIView *selfView=self.view;
    LoadingView *temploadingView =[LoadingView loadingViewInView:selfView];
    self.loadingView=temploadingView;
    
    [self performSelector:@selector(showloadingView) withObject:nil afterDelay:2];
    */
    
    /*if(![self NetworkStatus])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"You're not connected to Internet" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    else
    { 
        
        
        
        BpPromotionCell *selectedcell= (BpPromotionCell*)[tableView cellForRowAtIndexPath:indexPath];    
        //NSLog(@"%d",selectedcell.CAgendaEvent.AgendaEventID );
        
        
        AgendaEventDetailView *gAgendaEventDetailView=[[AgendaEventDetailView alloc] initWithNibName:@"AgendaEventDetailView" bundle:nil];
        
        NSString *sellectedAgendaID=[[NSString alloc]initWithFormat:@"%d",selectedcell.CAgendaEvent.AgendaEventID];
        gAgendaEventDetailView.gAgendaDetainID=sellectedAgendaID;
        gAgendaEventDetailView.agendaEvent=selectedcell.CAgendaEvent;
        gAgendaEventDetailView.title=@"Tupperware Brands Asia Pasific";
        
        [self.navigationController pushViewController:gAgendaEventDetailView animated:YES];*/
        
        /*BulletinBoardDetail *gBulletinBoardDetail=[[BulletinBoardDetail alloc] initWithNibName:@"BulletinBoardDetail" bundle:nil];
         gBulletinBoardDetail.BulletinID= selectedcell.bulletinMessage.ID;
         gBulletinBoardDetail.Bulletintitle=selectedcell.bulletinMessage.Title;
         gBulletinBoardDetail.Bulletintimedate=selectedcell.bulletinMessage.Time_Date;
         gBulletinBoardDetail.Bulletinbody=selectedcell.bulletinMessage.Body;
         gBulletinBoardDetail.Bulletinimagepath=selectedcell.bulletinMessage.ImagePath;
         
         
         gBulletinBoardDetail.title=@"Tupperware Brands Asia Pasific";
         
         [self.navigationController pushViewController:gBulletinBoardDetail animated:YES];*/
    //}
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

}



- (void)dealloc 
{
    [enlargeImagePath release];
    [loadingView release];
    [promtArr release];
    [tblPromotion release];
    [Bgimg release];
    [lblnoItem release];
    [super dealloc];
}


@end
