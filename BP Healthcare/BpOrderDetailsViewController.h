//
//  BpOrderDetailsViewController.h
//  BP Healthcare
//
//  Created by desmond on 13-1-9.
//
//

#import <UIKit/UIKit.h>
#import "BpPromotionItem.h"
#import "BpInsertNewOrderRequest.h"
#import "BpAppDelegate.h"
#import "NetworkHandler.h"
#import "BpUpdateOrderRequest.h"
#import "BpIpay88ViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface BpOrderDetailsViewController : UIViewController<NetworkHandlerDelegate, UITextFieldDelegate,UIPickerViewDelegate, UIPickerViewDataSource, UIActionSheetDelegate,ResultDelegate>

@property (retain, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (retain, nonatomic) IBOutlet UILabel *nameLabel;
@property (retain, nonatomic) IBOutlet UILabel *quantityLabel;
@property (retain, nonatomic) IBOutlet UIImageView *imageView;
@property BOOL insertedTransaction;

//personal & item field
@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *email;
@property (copy, nonatomic) NSString *contact;
@property (copy, nonatomic) NSString *fax;
@property (copy, nonatomic) NSString *amount;

@property (retain, nonatomic) IBOutlet UILabel *remarkLabel;
@property (retain, nonatomic) IBOutlet UILabel *priceLabel;
@property (retain, nonatomic) IBOutlet UITextField *nameField;
@property (retain, nonatomic) IBOutlet UITextField *emailField;
@property (retain, nonatomic) IBOutlet UITextField *contactField;
@property (retain, nonatomic) IBOutlet UITextField *faxField;
@property (retain, nonatomic) IBOutlet UIButton *selfPickButton;
@property (retain, nonatomic) IBOutlet UIButton *deliveryButton;
@property (retain, nonatomic) IBOutlet UILabel *remindLabel;
@property (retain, nonatomic) IBOutlet UIView *pickMethodView;
@property (retain, nonatomic) IBOutlet UIView *mainContentView;

@property (retain, nonatomic) IBOutlet UIView *selfPickView;
@property (retain, nonatomic) IBOutlet UIView *deliveryView;
@property (retain, nonatomic) IBOutlet UIButton *confirmButton;

//billing & shipping detail field
@property (retain, nonatomic) IBOutlet UITextField *billingAddressField1;
@property (retain, nonatomic) IBOutlet UITextField *billingAddressField2;
@property (retain, nonatomic) IBOutlet UITextField *billingTownField;
@property (retain, nonatomic) IBOutlet UITextField *billingPostcodeField;
@property (retain, nonatomic) IBOutlet UITextField *shippingAddressField1;
@property (retain, nonatomic) IBOutlet UITextField *shippingAddressField2;
@property (retain, nonatomic) IBOutlet UITextField *shippingTownField;
@property (retain, nonatomic) IBOutlet UITextField *shippingPostcodeField;
@property (retain, nonatomic) IBOutlet UIButton *sameAsBillingButton;

//self pick detail field
@property (retain, nonatomic) IBOutlet UITextField *stateField;


@property NSInteger itemsQuantity;
@property (retain,nonatomic) NSString *ReferenceNumber;
@property (retain,nonatomic) NSString *pickMehod;
@property BOOL sameAsBillingAddress;
@property (retain , nonatomic) BpPromotionItem *promotionItem;
@property (retain, nonatomic) UIImage *img;

@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *spinnerAnima;

-(IBAction)sameAsBillingAddress:(UIButton *)sender;
-(NSString *)dataFilePath;
-(IBAction)backgroundTap:(id)sender;
-(IBAction)addTargetToPickMethodView:(UIButton *)sender;
-(IBAction)onTouchup:(UIButton *)sender;
-(void)showPicker;
-(IBAction)confirmDeal:(UIButton *)sender;
-(IBAction)canceDeal:(UIButton *)sender;

//for picker use
@property(strong, nonatomic) IBOutlet UIPickerView *picker;
@property (strong, nonatomic) UIActionSheet *actionSheet;
@property(strong, nonatomic) NSDictionary *stateBranch;
@property(strong, nonatomic) NSArray *states;
@property(strong, nonatomic) NSArray *branchs;
@property(strong, nonatomic) NSString *state;
@property(strong, nonatomic) NSString *branch;
@property int branchRow;
@property int stateRow;

-(void)dismissActionSheet;  //

@end
