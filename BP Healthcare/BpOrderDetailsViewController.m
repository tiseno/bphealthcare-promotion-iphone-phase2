//
//  BpOrderDetailsViewController.m
//  BP Healthcare
//
//  Created by desmond on 13-1-9.

#import "BpOrderDetailsViewController.h"
#define personalDetailFileName @"personalDetail.plist"
#define stateComponent 0
#define branchComponent 1

@interface BpOrderDetailsViewController ()

@end

@implementation BpOrderDetailsViewController
@synthesize ReferenceNumber;
@synthesize itemsQuantity,promotionItem,img, insertedTransaction;
@synthesize stateBranch,states,branchs,picker, actionSheet , branch ,state,branchRow,stateRow, spinnerAnima;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    BpOrderDetailsViewController *gBpLatestPromotionViewController=[[BpOrderDetailsViewController alloc] initWithNibName:@"BpOrderDetailsViewController" bundle:nil];
    
    /**/UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];
    leftButton.frame = CGRectMake(0, 0, 50, 33);
    [leftButton addTarget:self action:@selector(handleBack:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView: leftButton] autorelease];
    gBpLatestPromotionViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    
    self.navigationItem.title = @"Order Info";
    [leftButton release];
    [gBpLatestPromotionViewController release];

    
    // init all TextField
    [self initTextField:_nameField];
    [self initTextField:_emailField];
    [self initTextField:_contactField];
    [self initTextField:_faxField];
    [self initTextField:_billingAddressField1];
    [self initTextField:_billingAddressField2];
    [self initTextField:_billingPostcodeField];
    [self initTextField:_billingTownField];
    [self initTextField:_shippingAddressField1];
    [self initTextField:_shippingAddressField2];
    [self initTextField:_shippingPostcodeField];
    [self initTextField:_shippingTownField];
    [self initTextField:_stateField];
    
    [_stateField addTarget:self action:@selector(showPicker) forControlEvents:UIControlEventTouchUpInside];
    _stateField.tag = 1;

    //will auto fill up personal info if have save before
    NSString *filePath = [self dataFilePath];
    if([[NSFileManager defaultManager]  fileExistsAtPath:filePath]){
        NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:filePath];
        _nameField.text = [dict objectForKey:@"name"];
        _emailField.text = [dict objectForKey:@"email"];
        _contactField.text = [dict objectForKey:@"contact"];
        _faxField.text = [dict objectForKey:@"fax"];
        
        _billingAddressField1.text = [dict objectForKey:@"billingAddress1"];
        _billingAddressField2.text = [dict objectForKey:@"billingAddress2"];
        _billingPostcodeField.text = [dict objectForKey:@"billingPostcode"];
        _billingTownField.text = [dict objectForKey:@"billingTown"];
    }
    
    UIApplication *app = [UIApplication sharedApplication];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillResignActive) name:UIApplicationWillResignActiveNotification object:app];

    _sameAsBillingAddress = NO;
    
    _quantityLabel.text = [[NSString alloc] initWithFormat:@"%d",itemsQuantity];
    _nameLabel.text = promotionItem.Name;
    
    _remarkLabel.text = [[NSString alloc] initWithFormat:@"%d X %@",itemsQuantity,promotionItem.Name];
    
    _priceLabel.text = [[NSString alloc] initWithFormat:@"RM %d",(itemsQuantity* [promotionItem.DiscountPrice intValue])];
    
    _imageView.image = img;
    
    //addind main view to scroll view
    [_mainScrollView addSubview: _mainContentView];
    
    //read all branch and state from plist
    NSBundle *bundle = [NSBundle mainBundle];
    NSURL *plistURL = [bundle URLForResource:@"statedictionary" withExtension:@"plist"];
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfURL:plistURL];
    self.stateBranch = dictionary;
    
    NSArray *components = [self.stateBranch allKeys];
    NSArray *sorted = [components sortedArrayUsingSelector:@selector(compare:)];
    
    self.states = sorted;
    
    NSString *selectedState = [self.states objectAtIndex:0];
    NSArray *array = [stateBranch objectForKey:selectedState];
    
    self.branchs = array;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initTextField:(UITextField *)textField{
//    textField.borderStyle = UITextBorderStyleRoundedRect;
    textField.delegate = self;
}

- (void)dealloc {
    [_remarkLabel release];
    [_priceLabel release];
    [_nameField release];
    [_emailField release];
    [_contactField release];
    [_faxField release];
    [_nameLabel release];
    [_name release];
    [_email release];
    [_contact release];
    [_fax release];
    [_amount release];
    [_quantityLabel release];
    [_imageView release];
    [_selfPickButton release];
    [_deliveryButton release];
    [_remindLabel release];
    [_pickMethodView release];
    [_selfPickView release];
    [_mainScrollView release];
    [_mainContentView release];
    [_deliveryView release];
    [_billingAddressField1 release];
    [_billingTownField release];
    [_billingPostcodeField release];
    [_shippingAddressField1 release];
    [_shippingAddressField2 release];
    [_shippingTownField release];
    [_shippingPostcodeField release];
    [_stateField release];
    [_sameAsBillingButton release];
    [spinnerAnima release];
    [_confirmButton release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setRemarkLabel:nil];
    [self setPriceLabel:nil];
    [self setNameField:nil];
    [self setEmailField:nil];
    [self setContactField:nil];
    [self setFaxField:nil];
    [self setNameLabel:nil];
    [self setName:nil];
    [self setEmail:nil];
    [self setContact:nil];
    [self setFax:nil];
    [self setAmount:nil];
    [self setQuantityLabel:nil];
    [self setImageView:nil];
    [self setSelfPickButton:nil];
    [self setDeliveryButton:nil];
    [self setRemindLabel:nil];
    [self setPickMethodView:nil];
    [self setSelfPickView:nil];
    [self setMainScrollView:nil];
    [self setMainContentView:nil];
    [self setDeliveryView:nil];
    [self setBillingAddressField1:nil];
    [self setBillingTownField:nil];
    [self setBillingPostcodeField:nil];
    [self setShippingAddressField1:nil];
    [self setShippingAddressField2:nil];
    [self setShippingTownField:nil];
    [self setShippingPostcodeField:nil];
    [self setStateField:nil];
    [self setSameAsBillingButton:nil];
    [self setSpinnerAnima:nil];
    [self setConfirmButton:nil];
    [super viewDidUnload];
}

- (IBAction)sameAsBillingAddress:(UIButton *)sender {
    if(!_sameAsBillingAddress){
        [self performSelector:@selector(highlightButton:) withObject:sender afterDelay:0.0];
        [self disableTextField:_shippingAddressField1 text:_billingAddressField1.text enable:NO];
        [self disableTextField:_shippingAddressField2 text:_billingAddressField2.text enable:NO];
        [self disableTextField:_shippingPostcodeField text:_billingPostcodeField.text enable:NO];
        [self disableTextField:_shippingTownField text:_billingTownField.text enable:NO];
        _sameAsBillingAddress = YES;
    }else{
        [self performSelector:@selector(unHighlightButton:) withObject:sender afterDelay:0.0];
        [self disableTextField:_shippingAddressField1 text:@"" enable:YES];
        [self disableTextField:_shippingAddressField2 text:@"" enable:YES];
        [self disableTextField:_shippingPostcodeField text:@""enable:YES];
        [self disableTextField:_shippingTownField text:@"" enable:YES];
        _sameAsBillingAddress = NO;
    }
}

-(NSString *)dataFilePath{
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [path objectAtIndex:0];
    
    return [documentsDirectory stringByAppendingPathComponent:personalDetailFileName];
}

-(void)disableTextField:(UITextField *)sender text:(NSString *)text enable:(BOOL) flag{
//    sender.enabled = flag;
    sender.text = text;
    if(flag)
        sender.textColor =[UIColor blackColor];
    else
        sender.textColor =[UIColor blackColor];
}

-(IBAction)backgroundTap:(id)sender{
    [_nameField resignFirstResponder];
    [_emailField resignFirstResponder];
    [_contactField resignFirstResponder];
    [_faxField resignFirstResponder];
    
    [_billingAddressField1 resignFirstResponder];
    [_billingAddressField2 resignFirstResponder];
    [_billingPostcodeField resignFirstResponder];
    [_billingTownField resignFirstResponder];
    [_shippingAddressField1 resignFirstResponder];
    [_shippingAddressField2 resignFirstResponder];
    [_shippingPostcodeField resignFirstResponder];
    [_shippingTownField resignFirstResponder];
    
    [_stateField resignFirstResponder];
}

//when focusing text field will scroll up the view
- (void)textFieldDidBeginEditing:(UITextField *)textField{

    if(textField.tag == 1){
        [self showPicker];
        [textField resignFirstResponder];
        
    }else if(textField.tag >=2 && textField.tag <= 5){
        CGFloat keyboardHeight = 216.0f;

        if (self.view.frame.size.height - keyboardHeight <= textField.frame.origin.y + textField.frame.size.height + 80) {
            CGFloat y = textField.frame.origin.y - (self.view.frame.size.height - keyboardHeight - textField.frame.size.height- 80 - 5);
            [UIView beginAnimations:@"srcollView" context:nil];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
            [UIView setAnimationDuration:0.275f];
            self.view.frame = CGRectMake(self.view.frame.origin.x, -y, self.view.frame.size.width, self.view.frame.size.height);
            [UIView commitAnimations];

    }
    }else{
    
        CGFloat keyboardHeight = 216.0f;
        if (self.view.frame.size.height - keyboardHeight <= textField.frame.origin.y + textField.frame.size.height) {
            CGFloat y = textField.frame.origin.y - (self.view.frame.size.height - keyboardHeight - textField.frame.size.height - 5);
            [UIView beginAnimations:@"srcollView" context:nil];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
            [UIView setAnimationDuration:0.275f];
            self.view.frame = CGRectMake(self.view.frame.origin.x, -y, self.view.frame.size.width, self.view.frame.size.height);
            [UIView commitAnimations];
    }
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return NO;
}

//when finish editing view will restore 
- (void)textFieldDidEndEditing:(UITextField *)textField{
    [UIView beginAnimations:@"srcollView" context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.275f];
    self.view.frame = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
    [UIView commitAnimations];
}

-(void)applicationWillResignActive{
    //when user press home button will run to this 
}

#pragma This method will add a view(self pick or delivery view) inside the socondView inside mainview
-(IBAction)addTargetToPickMethodView:(UIButton *)sender{
     NSString *title = [sender titleForState:UIControlStateNormal];
    
    //if title is selfPick, add selfPickView to pickMethodView area 
    if([title compare:@"selfPick"] == 0){
        _pickMehod = @"Self Pick";
        [sender setHighlighted:YES];
        //calculate the scrool view size
        CGSize previousPickViewSize = _pickMethodView.frame.size;
        CGSize secondView = _selfPickView.frame.size;
        
        //remove previous view and add current need view
        [_deliveryView removeFromSuperview];
        [_pickMethodView addSubview:_selfPickView];
        
        CGRect pickViewFrame = _pickMethodView.frame;
        pickViewFrame.size.height = secondView.height;
        [_pickMethodView setFrame:pickViewFrame];
        
        CGSize currentPickViewSize = _pickMethodView.frame.size;
        
        CGRect mainViewFrame = _mainContentView.frame;
        mainViewFrame.size.height += currentPickViewSize.height - previousPickViewSize.height;
        [_mainContentView setFrame:mainViewFrame];
        
        [_mainScrollView setContentSize:_mainContentView.frame.size];
        
    }else{
        _pickMehod = @"Delivery";
        CGSize previousPickViewSize = _pickMethodView.frame.size;
        CGSize secondView = _deliveryView.frame.size;
        
        [_selfPickView removeFromSuperview];
        [_pickMethodView addSubview:_deliveryView];

        CGRect pickViewFrame = _pickMethodView.frame;
        pickViewFrame.size.height = secondView.height;
        [_pickMethodView setFrame:pickViewFrame];
        
        CGSize currentPickViewSize = _pickMethodView.frame.size;
        
        CGRect mainViewFrame = _mainContentView.frame;
        mainViewFrame.size.height += currentPickViewSize.height - previousPickViewSize.height;
        [_mainContentView setFrame:mainViewFrame];

        [_mainScrollView setContentSize:_mainContentView.frame.size];
    }
}

- (void)highlightButton:(UIButton *)b {
//    [b setHighlighted:YES];
    UIImage *img = [UIImage imageNamed:@"radio_checked.png"];
    [b setImage:img forState:UIControlStateNormal];
}

- (void)unHighlightButton:(UIButton *)b {
    UIImage *img = [UIImage imageNamed:@"radio.png"];
    [b setImage:img forState:UIControlStateNormal];
}

- (IBAction)onTouchup:(UIButton *)sender {
    NSString *title = [sender titleForState:UIControlStateNormal];
    
    //if title is selfPick, add selfPickView to pickMethodView area
    if([title compare:@"selfPick"] == 0){
        _pickMehod = @"Self Pick";
        [self performSelector:@selector(highlightButton:) withObject:_selfPickButton afterDelay:0.0];
        [self performSelector:@selector(unHighlightButton:) withObject:_deliveryButton afterDelay:0.0];
    }else{
        _pickMehod = @"Delivery";
        [self performSelector:@selector(highlightButton:) withObject:_deliveryButton afterDelay:0.0];
        [self performSelector:@selector(unHighlightButton:) withObject:_selfPickButton afterDelay:0.0];
    }
}

- (void) handleBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


//show action sheet that contant picker 
- (void) showPicker{
    actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                              delegate:nil
                                     cancelButtonTitle:nil
                                destructiveButtonTitle:nil
                                     otherButtonTitles:nil];
    
    [actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    
    CGRect pickerFrame = CGRectMake(0, 40, 0, 0);
    
    picker = [[UIPickerView alloc] initWithFrame:pickerFrame];
    picker.showsSelectionIndicator = YES;
    picker.dataSource = self;
    picker.delegate = self;
    [picker selectRow:branchRow inComponent:branchComponent animated:YES];
    [picker selectRow:stateRow inComponent:stateComponent animated:YES];
    
    [actionSheet addSubview:picker];
    
    UISegmentedControl *closeButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Done"]];
    closeButton.momentary = YES;
    closeButton.frame = CGRectMake(260, 7.0f, 50.0f, 30.0f);
    closeButton.segmentedControlStyle = UISegmentedControlStyleBar;
    closeButton.tintColor = [UIColor blackColor];
    [closeButton addTarget:self action:@selector(dismissActionSheet) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:closeButton];
    
    [actionSheet showInView:[[UIApplication sharedApplication] keyWindow]];
    
    [actionSheet setBounds:CGRectMake(0, 0, 320, 485)];
}

-(void)dismissActionSheet{
    [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
    NSInteger _stateRow = [picker selectedRowInComponent:stateComponent];
    NSInteger _zipRow = [picker selectedRowInComponent:branchComponent];
    self.stateRow = _stateRow;
    self.branchRow = _zipRow;
    
     NSLog(@"selectedRowInComponent: %d , %d",_stateRow, _zipRow);
    state = [states objectAtIndex:_stateRow];
    branch = [branchs objectAtIndex:_zipRow];
    
    _stateField.text = [[NSString alloc] initWithFormat:@"%@, %@", state, branch];
}

- (IBAction)confirmDeal:(UIButton *)sender {
    if([self haveFieldEmpty]){  //check got empty field
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Empty Fields"
                                                        message:@"You must fill in all the fields."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
        
    }else{
        if(![self validEmail:_emailField.text]){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Format Error!!"
                                                            message:@"Email format incorrect."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];

        }
        else{
            [self writeUserInfoToFile];
            _confirmButton = sender;
            _confirmButton.enabled = NO;
            _confirmButton.alpha = 0.5;
            [spinnerAnima startAnimating];
                   
            if(!insertedTransaction){
                NetworkHandler *networkHandler=[[NetworkHandler alloc] init];
                networkHandler.webserviceURLType = @"service.asmx";
                BpInsertNewOrderRequest *insertSet= [[BpInsertNewOrderRequest alloc] init];
                [self fillOrderDetailTo:insertSet];
                [networkHandler setDelegate:self];
                [networkHandler request:insertSet];
                
            }else{
                [self startPayment];
            }
        }
    }
}

-(void)disableButtonAnima{
    _confirmButton.enabled = YES;
    _confirmButton.alpha = 1.0;
    [spinnerAnima stopAnimating];
}

-(BOOL)haveFieldEmpty{
    NSArray *textFieldTagNumbers;
    if([_pickMehod isEqualToString:@"Self Pick"]){
        textFieldTagNumbers = [[NSArray alloc]initWithObjects:
                               @"2",@"3",@"4",@"5",    //customer info textfield
                               @"1"   //self pick
                               ,nil];
    }else{
        textFieldTagNumbers = [[NSArray alloc]initWithObjects:
                                            @"2",@"3",@"4",@"5",    //customer info textfield
                                            @"10",@"11",@"12",@"13",    //billing address
                                            @"16",@"17",@"18",@"19"   //shipping address
                                           ,nil];
    }
    UITextField *textField;
    for(NSString *tag in textFieldTagNumbers){
        textField = (UITextField *)[self.view viewWithTag:[tag integerValue]];
        if([textField.text isEqualToString:@""]){
            return YES;
        }
    }
    return NO;
}

#pragma NetworkHandler delegate method
-(void)handleRecievedResponseMessage:(XMLResponse*)responseMessage
{
    if([responseMessage isKindOfClass:[BpInsertNewOrderResponse class]])
    {
        NSArray *msgArr=[[[NSArray alloc]init]autorelease];
        
        msgArr=((BpInsertNewOrderResponse *)responseMessage).ReferenceArr;
        
        for (BpReferenceNo *item in msgArr)
        {
            insertedTransaction = YES;
            ReferenceNumber = item.ReferenceNumber;          
        }
        
    [self startPayment];
    }else {
        /**/UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Warning!" message:@"Server down. Please try again later. Thanks you." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertView show];
        [alertView release];
    }
    
}

-(void)startPayment{
    BpIpay88ViewController *gBpIpay88ViewController = [[BpIpay88ViewController alloc] initWithNibName:@"BpIpay88ViewController" bundle:nil];
    gBpIpay88ViewController.referenceNumber = ReferenceNumber;
    gBpIpay88ViewController.resultDelegate = self;
    
    UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:gBpIpay88ViewController];
    
    [self presentModalViewController:navBar animated:YES];
    
    [gBpIpay88ViewController release];
    [navBar release];


}

-(void)writeUserInfoToFile{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:_nameField.text forKey:@"name"];
    [dict setObject:_emailField.text forKey:@"email"];
    [dict setObject:_contactField.text forKey:@"contact"];
    [dict setObject:_faxField.text forKey:@"fax"];
    
    [dict setObject:_billingAddressField1.text forKey:@"billingAddress1"];
    [dict setObject:_billingAddressField2.text forKey:@"billingAddress2"];
    [dict setObject:_billingPostcodeField.text forKey:@"billingPostcode"];
    [dict setObject:_billingTownField.text forKey:@"billingTown"];
    
    [dict setObject:promotionItem.DiscountPrice forKey:@"lastBillingAmount"];
    
    [dict setObject:promotionItem.Name forKey:@"lastBuyingProduct"];
    
    [dict setObject:[NSString stringWithFormat:@"%d", itemsQuantity] forKey:@"productQuantity"];
    
    [dict writeToFile:[self dataFilePath] atomically:YES];
    [dict release];
}

-(void)fillOrderDetailTo:(BpInsertNewOrderRequest *)request{
    BpAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    
    request.name = _nameField.text;
    request.email = _emailField.text;
    request.tokendevice = appDelegate.TokenString;
    request.contact = _contactField.text;
    request.fax = _faxField.text;
    request.billadd1 = _billingAddressField1.text;
    request.billadd2 = _billingAddressField2.text;
    request.billpostcode = _billingPostcodeField.text;
    request.billtown = _billingTownField.text;
    request.shipadd1 = _shippingAddressField1.text;
    request.shipadd2 = _shippingAddressField2.text;
    request.shippostcode = _shippingPostcodeField.text;
    request.shiptown = _shippingTownField.text;
    request.remark = _remarkLabel.text;
    request.currency = @"MYR";
    request.amount = _priceLabel.text;
    request.itemName = promotionItem.Name;
    request.deliverymethod = _pickMehod;
    request.pickupbranch = branch;
    request.pickupstate = state;
}

- (IBAction)canceDeal:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark-
#pragma mark Picker Data Source Methods
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 2;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (component == branchComponent) {
        return [self.branchs count];
    }else
        return [self.states count];
}

#pragma mark Picker Delegate Methods
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if(component == branchComponent){
        return [self.branchs objectAtIndex:row];
    }else
        return [self.states objectAtIndex:row];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if (component == stateComponent) {
        NSString *selectesState = [self.states objectAtIndex:row];
        NSLog(@"selectesState: %@",selectesState);
        NSArray *array = [stateBranch objectForKey:selectesState];
        self.branchs = array;
        [pickerView selectRow:0 inComponent:branchComponent animated:YES];
        [pickerView reloadComponent:branchComponent];
        
        [pickerView reloadAllComponents];
    }
}

-(CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component{
    if(component == branchComponent){
        return 150;
    }else return 150;
}

#pragma ipay88Result Delegate
-(void)paymentResult:(NSDictionary *)result{
    NSString *paymentStatus = [result objectForKey:@"paymentStatus"];
    
    BpUpdateOrderRequest *updateOrder = [[BpUpdateOrderRequest alloc] init];
    updateOrder.paymentstatus = [result objectForKey:@"paymentStatus"];
    updateOrder.referenceNo = [result objectForKey:@"referenceNo"];
    updateOrder.remark = [result objectForKey:@"remark"];
    updateOrder.transactionid = [result objectForKey:@"transactionid"];
    updateOrder.authcode = [result objectForKey:@"authcode"];
    updateOrder.amount = [result objectForKey:@"amount"];
    
    updateOrder.name = _nameField.text;
    updateOrder.email = _emailField.text;

    updateOrder.contact = _contactField.text;
    updateOrder.fax = _faxField.text;
    updateOrder.billadd1 = _billingAddressField1.text;
    updateOrder.billadd2 = _billingAddressField2.text;
    updateOrder.billpostcode = _billingPostcodeField.text;
    updateOrder.billtown = _billingTownField.text;
    updateOrder.shipadd1 = _shippingAddressField1.text;
    
    updateOrder.shipadd2 = _shippingAddressField2.text;
    updateOrder.shippostcode = _shippingPostcodeField.text;
    updateOrder.shiptown = _shippingTownField.text;
    updateOrder.remark = _remarkLabel.text;

    updateOrder.deliverymethod = _pickMehod;
    updateOrder.pickupbranch = branch;
    updateOrder.pickupstate = state;
    
    NSLog(@"ref: %@",updateOrder.referenceNo);
    
    NetworkHandler *networkHandler=[[NetworkHandler alloc] init];
    networkHandler.webserviceURLType = @"service.asmx";
    [networkHandler request:updateOrder];
    
    if([paymentStatus isEqualToString:@"Successful"]){
        NSLog(@"goto root");
        NSArray *controllers = self.navigationController.viewControllers;
        [self.navigationController popToViewController:[controllers objectAtIndex:1] animated:YES];
    }else if([paymentStatus isEqualToString:@"Failed"] ||
                 [paymentStatus isEqualToString:@"Not yet proccess"] ){
        //do nothing
    }
    [self disableButtonAnima];
}

-(BOOL) validEmail:(NSString*) emailString {
    NSString *regExPattern = @"^[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}$";
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
    NSLog(@"%i", regExMatches);
    if (regExMatches == 0) {
        return NO;
    } else
        return YES;
}


@end
