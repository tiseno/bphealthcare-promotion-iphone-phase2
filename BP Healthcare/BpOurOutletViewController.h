//
//  BpOurOutletViewController.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NetworkHandler.h"
#import "Reachability.h"
#import "BpOutlet.h"
#import "BpOutletRequest.h"
#import "BpOutletResponse.h"
#import <MapKit/MapKit.h> 
#import <CoreLocation/CoreLocation.h>
#import "BpOuletDetailsViewController.h"
#import "BpStore.h"
#import "BpAppDelegate.h"

@interface BpOurOutletViewController : UIViewController<NetworkHandlerDelegate,MKMapViewDelegate>{
    
}
@property (nonatomic, retain) NSString *SelectedState;
-(void)getOutlet;
@property (retain, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic, retain) NSArray *storesArray;
-(void)prepareToDisplay;
-(void)addAvailableAnnotaions;
@property (nonatomic, retain) NSArray *OutletArr;


@end
