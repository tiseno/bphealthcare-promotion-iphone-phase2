//
//  BpOurOutletViewController.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpOurOutletViewController.h"

@interface BpOurOutletViewController ()

@end

@implementation BpOurOutletViewController
@synthesize mapView;
@synthesize storesArray;
@synthesize OutletArr;
@synthesize SelectedState;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    BpOurOutletViewController *gBpOurOutletViewController=[[BpOurOutletViewController alloc] initWithNibName:@"BpOurOutletViewController" bundle:nil];
    /**/UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom]; 
    [leftButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];    
    leftButton.frame = CGRectMake(0, 0, 50, 33);
    [leftButton addTarget:self action:@selector(handleBack:) forControlEvents:UIControlEventTouchUpInside];   
    
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView: leftButton] autorelease]; 
    gBpOurOutletViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    [leftButton release];
    [gBpOurOutletViewController release];
    [self getOutlet];
    [self ViewMap];
}

-(void)ViewMap///:(NSArray*)store
{
    /*NSArray *storeArray;
    BpStore *store1=[[BpStore alloc] init];
    store1.storeName=@"Petronas";
    store1.storeAddress=@"59 Jalan Ampang,Kampung Datok Keramat";
    BpLocation *location=[[BpLocation alloc] initWithLatitude:3.161149 longitude:101.73020];
    store1.location=location;
    [location release];
    
    BpStore *store2=[[BpStore alloc] init];
    store2.storeName=@"Petronas";
    store2.storeAddress=@"Jalan Raja Muda Abdul Aziz, Kampung Baharu";
    location=[[BpLocation alloc] initWithLatitude:3.167919 longitude:101.711233];
    store2.location=location;
    [location release];
    
    BpStore *store3=[[BpStore alloc] init];
    store3.storeName=@"Petronas";
    store3.storeAddress=@"Jalan Kolam Air Lama, 68000 Ampang";
    location=[[BpLocation alloc] initWithLatitude:3.159959 longitude:101.75327];
    store3.location=location;
    [location release];
    
    BpStore *store4=[[BpStore alloc] init];
    store4.storeName=@"Petronas";
    store4.storeAddress=@"Jalan Lumba Kuda, Kuala Lumpur City Centre";
    location=[[BpLocation alloc] initWithLatitude:3.15825 longitude:101.715923];
    store4.location=location;
    [location release];
    
    BpStore *store5=[[BpStore alloc] init];
    store5.storeName=@"Petronas";
    store5.storeAddress=@"Jalan+Enggang, Taman Keramat, Kuala Lumpur";
    location=[[BpLocation alloc] initWithLatitude:3.160699 longitude:101.730309];
    store5.location=location;
    [location release];
    
    
    storeArray=[NSArray arrayWithObjects:store1,store2,store3,store4,store5, nil];
    [store1 release];
    [store2 release];
    [store3 release];
    [store4 release];*/
    
    
    //BpOurOutletViewController *stationLocator=[[BpOurOutletViewController alloc] initWithNibName:@"BpOurOutletViewController" bundle:nil];
    //self.storesArray=store;
    //stationLocator.title=@"Station Locator";
    //[self.navigationController pushViewController:stationLocator animated:YES];
    [self performSelector:@selector(prepareToDisplay) withObject:nil afterDelay:3];
    //[stationLocator release];
}

- (void)viewDidUnload
{
    [self setMapView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void) handleBack:(id)sender
{
    // do your custom handler code here
    
    // make sure you do this!
    // pop the controller
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)getOutlet
{
    
    NetworkHandler *networkHandler=[[NetworkHandler alloc] init];
    BpOutletRequest *loginset= [[BpOutletRequest alloc] initWithsState:SelectedState];
    NSLog(@"SelectedState--->%@",SelectedState);
    [networkHandler setDelegate:self];
    [networkHandler request:loginset];
    [loginset release];
    [networkHandler release];
}

-(void)handleRecievedResponseMessage:(XMLResponse*)responseMessage
{
    
    if([responseMessage isKindOfClass:[BpOutletResponse class]])
    {
        
        //NSString *strMessage=[NSString stringWithFormat:@"%@",((BpOutletResponse*)responseMessage).OutletArr];
        
        NSArray *msgArr=[[[NSArray alloc]init]autorelease];
        msgArr=((BpOutletResponse*)responseMessage).OutletArr;
        self.OutletArr=msgArr;
        //self.storesArray=msgArr;
        
        //NSLog(@"strMessage--->%@",strMessage);
        NSArray *storeArraya;
        
        NSMutableArray *BizEntity= [[[NSMutableArray alloc]init] autorelease];
         
        for (BpOutlet *item in msgArr) {
            //birthitem.resultCount;
            /*NSLog(@"resultCount--->%@",item.resultCount);
            NSLog(@"RowID--->%@",item.RowID);
            NSLog(@"Address_1--->%@",item.Address_1);
            
            NSLog(@"Address_2--->%@",item.Address_2);
            NSLog(@"Address_3--->%@",item.Address_3);
            NSLog(@"BranchName--->%@",item.BranchName);
            NSLog(@"Latitude--->%@",item.Latitude);
            NSLog(@"Longitude--->%@",item.Longitude);*/

            BpStore *store=[[BpStore alloc] init];
            store.storeName=item.BranchName;
            //store.storeAddress=@"59 Jalan Ampang,Kampung Datok Keramat";
            
            CLLocationManager *locationManager = [[CLLocationManager alloc] init];
            
            store.BranchName=item.BranchName;
            store.BranchImg=item.BranchImg;
            store.Address_1=item.Address_1;
            store.Address_2=item.Address_2;
            store.Address_3=item.Address_3;
            store.Tel=item.Tel;
            store.Fax=item.Fax;
            store.BusinessHourImg=item.BusinessHourImg;
            store.TypeDC=item.TypeDC;
            store.TypeLAB=item.TypeLAB;
            store.TypeHEARING=item.TypeHEARING;
            store.TypeDISPENSARY=item.TypeDISPENSARY;
            store.Latitude=item.Latitude;
            store.Longitude=item.Longitude;
            
            NSString *clatitute=[[NSString alloc]initWithFormat:@"%f",locationManager.location.coordinate.latitude];
            NSString *clongtitute=[[NSString alloc]initWithFormat:@"%f",locationManager.location.coordinate.longitude];
            store.currentLatitude=clatitute;
            store.currentLongitude=clongtitute;
            
            [clatitute release];
            [clongtitute release];
            [locationManager release];
            
            BpLocation *location=[[BpLocation alloc] initWithLatitude:[item.Latitude doubleValue] longitude:[item.Longitude doubleValue]];
            //BpLocation *location=[[BpLocation alloc] initWithLatitude:3.089951 longitude:101.68956];
            store.location=location;
            store.Latitude=item.Latitude;
            store.Longitude=item.Longitude;
            
            //NSLog(@"item.BranchName--->%@",item.BranchName);
            
            //NSLog(@"item.Latitude--->%0.7f",latitute);
            //NSLog(@"item.Longitude--->%0.7f",[item.Longitude doubleValue]);
            
            /*<BranchName>Melaka</BranchName>
             <Address_1>113 &amp; 114, Jalan Merdeka,</Address_1>
             <Address_2>Taman Melaka Raya, 75000 Melaka</Address_2>
             <Address_3 />
             <Tel>06-2869902</Tel>
             <Fax>06-2850296</Fax>
             <BusinessHourImg>http://www.bphealthcare.com/images/DC/BH_melaka.jpg</BusinessHourImg>
             <TypeDC>http://www.bphealthcare.com/images/icon_dc.jpg</TypeDC>
             <TypeLAB>http://www.bphealthcare.com/images/icon_dispensary.jpg</TypeLAB>
             <TypeHEARING>http://www.bphealthcare.com/images/icon_ha.jpg</TypeHEARING>
             <TypeDISPENSARY>http://www.bphealthcare.com/images/icon_lab.jpg</TypeDISPENSARY>
             <Latitude>2.185631</Latitude>
             <Longitude>102.261356</Longitude>*/

            
            
            [location release];
            
            [BizEntity addObject:store];
            [store release];
        }
        
        
        
        self.storesArray=BizEntity;
        
        //NSLog(@"BizEntity--->%@",BizEntity);
        //[self ViewMap:self.storesArray];
    }
    
}

-(void)prepareToDisplay
{
    if(mapView.annotations!=nil && [mapView.annotations count]>0)
	{
		[mapView removeAnnotations:mapView.annotations];
	}
    
    CLLocationCoordinate2D mapCenter;
    
    if([CLLocationManager locationServicesEnabled] &&
       [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied){
        CLLocationManager *locationManager = [[CLLocationManager alloc] init]; 
        //NSString *url = [NSString stringWithFormat: @"http://maps.google.com/maps?saddr=%f,%f&daddr=%@,%@", locationManager.location.coordinate.latitude, locationManager.location.coordinate.longitude, annot.Latitude, annot.Longitude]; 
        
        
        mapCenter.latitude=locationManager.location.coordinate.latitude;
        mapCenter.longitude=locationManager.location.coordinate.longitude;
        mapView.showsUserLocation=YES;
	}else{
        mapCenter.latitude=2.767478;
        mapCenter.longitude=105.644531;
        mapView.showsUserLocation=NO;
    }
    
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(mapCenter, 2000000,2000000);
    MKCoordinateRegion adjustedRegion = [mapView regionThatFits:viewRegion];
    [mapView setRegion:adjustedRegion animated:YES];
    mapView.showsUserLocation=YES;

	[self addAvailableAnnotaions];
}
-(void)addAvailableAnnotaions
{
	for (BpStore *store in self.storesArray) { 
        NSLog(@"store--->%@",store.storeName);
        [mapView addAnnotation:store];
	}
}
- (MKAnnotationView *)mapView:(MKMapView *)theMapView viewForAnnotation:(id <MKAnnotation>)annotation 
{
    if([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    //static NSString *placemarkIdentifier = @"Map Location Identifier";
	if ([annotation isKindOfClass:[BpStore class]])
	{
        //NSLog(@"BpStore class~");
		/*------------MKPinAnnotationView *annotationView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:placemarkIdentifier];
        
        if (annotationView == nil) { 
            //MKPinAnnotationView* customPinView = [[[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:placemarkIdentifier] autorelease];
            annotationView = [[[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:placemarkIdentifier ] autorelease];
            //UIImage *annotationImage=[UIImage imageNamed:@"icon1.png"];
            //annotationView.image=annotationImage;
            annotationView.annotation = annotation;
            NSLog(@"hello~");
        }
        else
        {
            annotationView.annotation = annotation;
        }
        //annotationView.pinColor = MKPinAnnotationColorPurple;
        
		annotationView.enabled = YES;
		annotationView.canShowCallout=YES;
		UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
		
     [rightButton addTarget:self action:@selector(loadStoreFromAnnotationView:) forControlEvents:UIControlEventTouchUpInside];
     
		annotationView.rightCalloutAccessoryView = rightButton;
		return annotationView;--------------*/
        
        static NSString* ClinicAnnotationIdentifier = @"Map Location Identifier";
        MKPinAnnotationView* pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:ClinicAnnotationIdentifier];
        if(!pinView)
        {
            //if an existing pin view was not available, create one
            MKPinAnnotationView* customPinView = [[[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:ClinicAnnotationIdentifier] autorelease];
            //customPinView.pinColor = MKPinAnnotationColorPurple;
            customPinView.animatesDrop = YES;
            customPinView.canShowCallout = YES;
            
            //add a detail disclosure button to the callout which will open a new view controller page
            //note : you can assign a specific call out accessory view, or as MKMapViewDelegate you can implement:
            //-(void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control;
            
            UIButton* rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            //[rightButton addTarget:self action:@selector(showDetails:) forControlEvents:UIControlEventTouchUpInside];
            customPinView.rightCalloutAccessoryView = rightButton;
            return customPinView;
        }
        else
        {
            pinView.annotation = annotation;
        }
        return pinView;
        
	}
    
	return nil;
     /*[rightButton addTarget:self action:@selector(loadStoreFromAnnotationView:) forControlEvents:UIControlEventTouchUpInside];*/
    /**/
    
}
-(void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)annotationView calloutAccessoryControlTapped:(UIControl *)control
{
    /**/BpOuletDetailsViewController *detailViewController = [[BpOuletDetailsViewController alloc] initWithNibName:@"BpOuletDetailsViewController" bundle:nil];
    //detailViewController.imagePath=@"faeilites.png";
    detailViewController.title=@"Info";
    [self.navigationController pushViewController:detailViewController animated:YES];
    [detailViewController release];
    
    
}

/**/- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    // Annotation is your custom class that holds information about the annotation
    if ([view.annotation isKindOfClass:[BpStore class]]) {
        BpStore *annot = view.annotation;
        NSLog(@"annot.storeName---->%@",annot.storeName);
        //NSArray *index = [self.storesArray indexOfObject:annot];
        //NSLog(@"index--->%@",index.lastObject);
        
        /*for (BpStore *items in annot) {
            NSLog(@"items.storeName--->%@",items.storeName);
        }
        CLLocationManager *locationManager = [[CLLocationManager alloc] init]; 

        NSString *url = [NSString stringWithFormat: @"http://maps.google.com/maps?saddr=%f,%f&daddr=%@,%@", locationManager.location.coordinate.latitude, locationManager.location.coordinate.longitude, annot.Latitude, annot.Longitude]; 
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        
        [locationManager release];*/
        
        BpAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
        appDelegate.storedetail=annot;
    }
}

- (void)dealloc 
{
    [SelectedState release];
    [OutletArr release];
    [storesArray release];
    [mapView release];
    [super dealloc];
}
@end
