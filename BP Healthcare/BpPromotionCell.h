//
//  BpPromotionCell.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BpPromotions.h"

@interface BpPromotionCell : UITableViewCell{
    
}

@property (nonatomic, retain) UIColor *lineColor;
@property (nonatomic,retain) UILabel *lblTitle;
@property (nonatomic, retain) UILabel *lblBodytext;
@property (nonatomic, retain) UILabel *lblBodyNoPictext;
@property (nonatomic, retain) UIImageView *imgsmallflag;
@property (nonatomic, retain) UIImageView *imgpicture;
@property (nonatomic, retain) BpPromotions *BpPromotionscell;
@property (nonatomic, retain)  UIButton *btnenlargeImage;
@property (nonatomic, retain) NSString *url;

@end
