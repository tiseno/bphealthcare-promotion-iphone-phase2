//
//  BpPromotionImageViewController.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 11/21/12.
//
//

#import "BpPromotionImageViewController.h"

@interface BpPromotionImageViewController ()

@end

@implementation BpPromotionImageViewController
@synthesize imgPath;
@synthesize promotionimgView;
@synthesize imgscrollView, loadingView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)dealloc
{
    [loadingView release];
    [imgPath release];
    [promotionimgView release];
    [imgscrollView release];
    [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    BpPromotionImageViewController *gBpPromotionImageViewController=[[BpPromotionImageViewController alloc] initWithNibName:@"BpPromotionImageViewController" bundle:nil];
    /**/UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];
    leftButton.frame = CGRectMake(0, 0, 50, 33);
    [leftButton addTarget:self action:@selector(handleBack:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView: leftButton] autorelease];
    gBpPromotionImageViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    self.navigationItem.title = _productName;
    [leftButton release];
    [gBpPromotionImageViewController release];

    
    UIView *selfView=self.view;
    LoadingView *temploadingView =[LoadingView loadingViewInView:selfView];
    self.loadingView=temploadingView;
    [self performSelector:@selector(getimagefromurl) withObject:nil afterDelay:1.0];
        
}

-(void)getimagefromurl
{
    
    UIImage *promotionimg=[[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imgPath]]];

    [self performSelector:@selector(getimage:) withObject:promotionimg afterDelay:1.0];

}

-(void)getimage:(UIImage *)img
{
    //promotionimgView=[[[UIImageView alloc] init] autorelease];
    //promotionimgView.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
     promotionimgView.image=img;
//    [promotionimgView setFrame:CGRectMake((self.view.frame.size.width/2)-(img.size.width/2), (self.view.frame.size.height/2)-(img.size.height/2), img.size.width, img.size.height)];
    
    
    [promotionimgView setFrame:CGRectMake(0,0,self.view.frame.size.width, self.view.frame.size.height)];
    

    
    [imgscrollView setBackgroundColor:[UIColor whiteColor]];
    [imgscrollView setCanCancelContentTouches:NO];
    imgscrollView.clipsToBounds = YES; // default is NO, we want to restrict drawing within our scrollview
    imgscrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
    //promotionimgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"PDF-icon.png"]];
    [imgscrollView addSubview:promotionimgView];
    [imgscrollView setContentSize:CGSizeMake(promotionimgView.frame.size.width, promotionimgView.frame.size.height)];
    imgscrollView.minimumZoomScale = 1;
    imgscrollView.maximumZoomScale = 3;
    imgscrollView.delegate = self;
    [imgscrollView setScrollEnabled:YES];
    [self.loadingView removeView];
    //NSLog(@"promotionimg.size.height>>>%f",promotionimgView.image.size.height);
}


-(UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return promotionimgView;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) handleBack:(id)sender
{
    // do your custom handler code here
    
    // make sure you do this!
    // pop the controller
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidUnload {
    [self setPromotionimgView:nil];
    [self setImgscrollView:nil];
    [super viewDidUnload];
}
@end
