//
//  BpTransaction.h
//  BP Healthcare
//
//  Created by desmond on 13-2-7.
//
//

#import <Foundation/Foundation.h>

@interface BpTransaction : NSObject

@property (copy, nonatomic) NSString *PickupBranch;
@property (copy, nonatomic) NSString *PickupState;
@property (copy, nonatomic) NSString *DeliveryMethod;
@property (copy, nonatomic) NSString *CreateDate;
@property (copy, nonatomic) NSString *ReferenceNo;
@property (copy, nonatomic) NSString *Amount;
@property (copy, nonatomic) NSString *Remark;
@property (copy, nonatomic) NSString *PaymentStatus;

@end
