//
//  BpTransaction.m
//  BP Healthcare
//
//  Created by desmond on 13-2-7.
//
//

#import "BpTransaction.h"

@implementation BpTransaction
@synthesize Amount,CreateDate,DeliveryMethod,PaymentStatus,PickupBranch,PickupState,ReferenceNo,Remark;

-(void)dealloc
{
    [Amount release];
    [CreateDate release];
    [DeliveryMethod release];
    [PaymentStatus release];
    [PickupBranch release];
    [PickupState release];
    [ReferenceNo release];
    [Remark release];    
    [super dealloc];
}


@end
