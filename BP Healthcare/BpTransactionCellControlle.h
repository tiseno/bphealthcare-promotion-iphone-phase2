//
//  BpTransactionCellControlle.h
//  BP Healthcare
//
//  Created by desmond on 13-2-7.
//
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface BpTransactionCellControlle : UITableViewCell

@property (retain, nonatomic) IBOutlet UILabel *labelTitle;
@property (retain, nonatomic) IBOutlet UILabel *labelRefNo;
@property (retain, nonatomic) IBOutlet UILabel *labelPrice;
@property (retain, nonatomic) IBOutlet UILabel *labelDate;
@property (retain, nonatomic) IBOutlet UIImageView *image;
@property (retain, nonatomic) IBOutlet UILabel *labelPickup;
@property (retain, nonatomic) IBOutlet UILabel *labelStatus;

-(void)setImageSrc:(NSString *)data UILabel:(UIImageView *)image;
-(void)setData:(NSString *)data UILabel:(UILabel *)label;
@end
