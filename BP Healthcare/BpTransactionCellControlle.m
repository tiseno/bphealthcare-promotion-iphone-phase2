//
//  BpTransactionCellControlle.m
//  BP Healthcare
//
//  Created by desmond on 13-2-7.
//
//

#import "BpTransactionCellControlle.h"

@interface BpTransactionCellControlle ()

@end

@implementation BpTransactionCellControlle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setData:(NSString *)data UILabel:(UILabel *)label{
    label.text = data;
}

-(void)setImageSrc:(NSString *)data UILabel:(UIImageView *)image{

}

- (void)dealloc {
    [_labelTitle release];
    [_labelRefNo release];
    [_labelPrice release];
    [_labelDate release];
    [_image release];
    [_labelPickup release];
    [_labelStatus release];
    [super dealloc];
}
@end
