//
//  BpTransactionViewController.h
//  BP Healthcare
//
//  Created by desmond on 13-2-7.
//
//

#import <UIKit/UIKit.h>
#import "BpTransactionCellControlle.h"
#import "NetworkHandler.h"
#import "LoadingView.h"
#import "BpGetAllTransactionsRequest.h"
#import "BpGetAllTransactionResponse.h"
#import "BpTransaction.h"

@interface BpTransactionViewController : UIViewController <NetworkHandlerDelegate,UITableViewDelegate, UITableViewDataSource>{
    BpTransactionCellControlle *cell;
    int indeximg;
}

-(void)getTransactions;
@property (retain, nonatomic) IBOutlet UITableView *tblTransaction;
@property (nonatomic, retain) NSArray *promtArr;
@property (nonatomic, retain) LoadingView *loadingView;
@property (nonatomic, retain) NSString *enlargeImagePath;
@property (retain, nonatomic) IBOutlet UIImageView *Bgimg;
@property (retain, nonatomic) IBOutlet UILabel *lblnoItem;

@end
