//
//  BpTransactionViewController.m
//  BP Healthcare
//
//  Created by desmond on 13-2-7.
//
//

#import "BpTransactionViewController.h"

@interface BpTransactionViewController ()

@end

@implementation BpTransactionViewController
@synthesize Bgimg, tblTransaction, lblnoItem, loadingView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    BpTransactionViewController *gBpTransactionViewController=[[BpTransactionViewController alloc] initWithNibName:@"BpTransactionViewController" bundle:nil];
    
    self.navigationItem.title = @"Purchase History";
    
    /**/UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];
    leftButton.frame = CGRectMake(0, 0, 50, 33);
    [leftButton addTarget:self action:@selector(handleBack:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView: leftButton] autorelease];
    gBpTransactionViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    
    [leftButton release];
    [gBpTransactionViewController release];
    
    [self getTransactions];
    lblnoItem.textColor = [UIColor colorWithRed:(160/255.0) green:(1/255.0) blue:(4/255.0) alpha:1.0];
    
    UIView *selfView=self.view;
    LoadingView *temploadingView =[LoadingView loadingViewInView:selfView];
    self.loadingView=temploadingView;
    
}

- (void)viewDidUnload
{
    [self setTblTransaction:nil];
    [self setBgimg:nil];
    [self setLblnoItem:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void) handleBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) handleHistory:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

/**/-(void)getTransactions
{
    NetworkHandler *networkHandler2=[[NetworkHandler alloc] init];
    networkHandler2.webserviceURLType = @"service.asmx";
    BpGetAllTransactionsRequest *requestset2= [[BpGetAllTransactionsRequest alloc] init];
    [networkHandler2 setDelegate:self];
    [networkHandler2 request:requestset2];
    
}

-(void)handleRecievedResponseMessage:(XMLResponse*)responseMessage
{
    NSLog(@"responseMessage class: %@",[responseMessage class]);
    if([responseMessage isKindOfClass:[BpGetAllTransactionResponse class]])
    {
        NSArray *msgArr=[[[NSArray alloc]init]autorelease];
        msgArr=((BpGetAllTransactionResponse *)responseMessage).TransactionArr;
        
        self.promtArr=msgArr;
        NSLog(@"responseMessage class: %d",[self.promtArr count]);
        for (BpTransaction *item in self.promtArr)
        {
            if ([self.promtArr count] > 0)
            {
                
                Bgimg.hidden=YES;
                lblnoItem.hidden=YES;
                
            }else
            {
                Bgimg.hidden=NO;
                lblnoItem.hidden=NO;
            }
        }
        
        [tblTransaction reloadData];
        [self.loadingView removeView];
        
    }else {
        /**/UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Warning!" message:@"Server down. Please try again later. Thanks you." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertView show];
        [alertView release];
        
        [self.loadingView removeView];
    }
    
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath: (NSIndexPath *) indexPath {
    cell = (BpTransactionCellControlle *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.frame.size.height;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.promtArr count];
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{   
    static NSString *CellTableIdentifier = @"TransactionCellIdentifier";
    
    [tableView registerNib:[UINib nibWithNibName:@"BpTransactionCellControlle" bundle:nil] forCellReuseIdentifier:CellTableIdentifier];
    
    BpTransactionCellControlle *cell = [tableView dequeueReusableCellWithIdentifier:CellTableIdentifier];
    
    NSUInteger row = [indexPath row];

    BpTransaction* classBpTransaction =[self.promtArr objectAtIndex:row];
    
    NSString *Price = [[NSString alloc] initWithFormat:@"RM %@",classBpTransaction.Amount];
    [cell setData:Price  UILabel:cell.labelPrice];
    [cell setData:classBpTransaction.CreateDate UILabel:cell.labelDate];
    [cell setData:classBpTransaction.Remark  UILabel:cell.labelTitle];
    [cell setData:classBpTransaction.ReferenceNo  UILabel:cell.labelRefNo];
    [cell setData:classBpTransaction.PaymentStatus  UILabel:cell.labelStatus];
    
    if([classBpTransaction.DeliveryMethod isEqualToString:@"Delivery"]){
        [cell setData:@"Delivery"  UILabel:cell.labelPickup];
    }else{
        NSString *pickupAddress = [[NSString alloc] initWithFormat:@"%@, %@",classBpTransaction.PickupState, classBpTransaction.PickupBranch];
        [cell setData:pickupAddress  UILabel:cell.labelPickup];
    }
    
    if([classBpTransaction.PaymentStatus isEqualToString:@"Failed"]){
        [cell.image setImage:[UIImage imageNamed:@"icon_failed"]];
    }else{
        [cell.image setImage:[UIImage imageNamed:@"icon_success"]];
    }
        return cell;
}

-(void)showloadingView
{
    [self.loadingView removeView];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    NSUInteger row = [indexPath row];
//    BpPromotionItem* classBpPromotions=[self.promtArr objectAtIndex:row];
//    
//    BpItemDetailsViewController *gBpItemViewController = [[BpItemDetailsViewController alloc] initWithNibName:@"BpItemDetailsViewController" bundle:nil];
//    gBpItemViewController.promotionItem = classBpPromotions;
//    [self.navigationController pushViewController:gBpItemViewController animated:YES];
//    [gBpItemViewController release];
//    
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}



- (void)dealloc
{
    [loadingView release];
//    [promtArr release];
    [tblTransaction release];
    [Bgimg release];
    [lblnoItem release];
    [super dealloc];
}


@end
