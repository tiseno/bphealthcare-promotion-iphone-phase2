//
//  BpUpdateOrderRequest.h
//  BP Healthcare
//
//  Created by desmond on 13-1-23.
//
//

#import "XMLRequest.h"

@interface BpUpdateOrderRequest : XMLRequest

@property (retain, nonatomic) NSString *referenceNo;
@property (retain, nonatomic) NSString *remark;
@property (retain, nonatomic) NSString *transactionid;
@property (retain, nonatomic) NSString *authcode;
@property (retain, nonatomic) NSString *amount;
@property (retain, nonatomic) NSString *paymentstatus;

@property (retain, nonatomic) NSString *name;
@property (retain, nonatomic) NSString *email;
@property (retain, nonatomic) NSString *contact;
@property (retain, nonatomic) NSString *fax;

@property (retain, nonatomic) NSString *billadd1;
@property (retain, nonatomic) NSString *billadd2;
@property (retain, nonatomic) NSString *billtown;
@property (retain, nonatomic) NSString *billpostcode;

@property (retain, nonatomic) NSString *shipadd1;
@property (retain, nonatomic) NSString *shipadd2;
@property (retain, nonatomic) NSString *shiptown;
@property (retain, nonatomic) NSString *shippostcode;

@property (retain, nonatomic) NSString *deliverymethod;

@property (retain, nonatomic) NSString *pickupstate;
@property (retain, nonatomic) NSString *pickupbranch;

@end
