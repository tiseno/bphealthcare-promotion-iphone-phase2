//
//  BpUpdateOrderRequest.m
//  BP Healthcare
//
//  Created by desmond on 13-1-23.
//
//

#import "BpUpdateOrderRequest.h"

@implementation BpUpdateOrderRequest

-(id)init
{
    self=[super init];
    if(self)
    {
        self.SOAPAction=@"UpdateOrderByRefID";
        self.requestType=WebserviceRequest;
    }
    return self;
}


-(NSString*) generateHTTPPostMessage
{

    NSString *postMsg = [NSString stringWithFormat:
                         @"referenceNo=%@"
                         "&remark=%@"
                         "&transactionid=%@"
                         "&authcode=%@"
                         "&amount=%@"
                         "&paymentstatus=%@"
                         
                         "&name=%@"
                         "&email=%@"
                         "&contact=%@"
                         "&fax=%@"
                         
                         "&billadd1=%@"
                         "&billadd2=%@"
                         "&billtown=%@"
                         "&billpostcode=%@"
                         
                         "&shipadd1=%@"
                         "&shipadd2=%@"
                         "&shiptown=%@"
                         "&shippostcode=%@"
                         
                         "&deliverymethod=%@"
                         "&pickupstate=%@"
                         "&pickupbranch=%@"
                         ,
                         _referenceNo,_remark,
                         _transactionid,_authcode,
                         _amount,_paymentstatus,
                         _name,_email,_contact,_fax,
                         _billadd1,_billadd2,_billtown,_billpostcode,
                         _shipadd1,_shipadd2,_shiptown,_shippostcode,
                         _deliverymethod,_pickupstate,_pickupbranch];
    //    NSLog(@"postMsg-----> %@",postMsg);
    return postMsg;
}


@end
