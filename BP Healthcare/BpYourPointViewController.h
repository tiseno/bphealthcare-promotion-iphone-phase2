//
//  BpYourPointViewController.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "NetworkHandler.h"
#import "BpMyPoint.h"
#import "BpMyPointRequest.h"
#import "BploginResponse.h"
#import "LoadingView.h"
#import "BpAppDelegate.h"

@interface BpYourPointViewController : UIViewController<NetworkHandlerDelegate>{
    
}

@property (retain, nonatomic) IBOutlet UITableView *yourpointtable;
@property(nonatomic,retain) UITextField *textfieldName;
@property (retain, nonatomic) IBOutlet UILabel *lblRewardPoint;
@property (retain, nonatomic) IBOutlet UILabel *lblRebatePoint;
@property (nonatomic, retain) LoadingView *loadingView;
-(void)getMyPoint:(NSString*)icNum;

@end
