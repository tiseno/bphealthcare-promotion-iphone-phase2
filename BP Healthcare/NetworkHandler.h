//
//  NetworkHandler.h
//  Holiday Villa
//
//  Created by Jermin Bazazian on 4/14/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMLRequest.h"
#import "ResponseTranslator.h"
#import "NetworkHandlerDelegate.h"


@interface NetworkHandler : NSObject {
    NSMutableData *webData;
    NSMutableString *soapResults;
    NSURLConnection *conn;
    
    id<NetworkHandlerDelegate> _delegate;
}
@property (nonatomic, retain) NSMutableData *webData;
@property (nonatomic, retain) NSMutableString *soapResults;
@property (nonatomic, retain) NSURLConnection *conn;

@property (nonatomic,retain) NSString *webserviceURLType;

- (id<NetworkHandlerDelegate>)delegate;
- (void)setDelegate:(id<NetworkHandlerDelegate>)new_delegate;

-(void)request:(XMLRequest*)xmlRequest;
@end
