//
//  ResponseTranslator.m
//  KDSWSLayer
//
//  Created by Jermin Bazazian on 12/5/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import "ResponseTranslator.h"


@implementation ResponseTranslator
-(XMLResponse*)translate:(NSData*) xmlData
{
    XMLResponse *message=nil;
    NSError *error;
    /*if([xmlData length]!=0)
    {
        
    }*/ 
    
    GDataXMLDocument *doc =[[GDataXMLDocument alloc]initWithData:xmlData options:0 error:&error ];
    
    //NSLog(@"doc.rootElement--->%@",doc.rootElement);
    
    
    if (doc) {
        if ([[doc.rootElement name] isEqualToString:@"ArrayOfBizEntityBirthdayTreatImage"]) 
        {
            message =[self translateBirthday:xmlData];
            
        }else if ([[doc.rootElement name] isEqualToString:@"ArrayOfBizEntityCheckUp"]) 
        {
            message =[self translateCheckUp:xmlData];
            
        }else if ([[doc.rootElement name] isEqualToString:@"ArrayOfBizEntityFestival"]) 
        {
            message =[self translateFestival:xmlData];
            
        }else if ([[doc.rootElement name] isEqualToString:@"ArrayOfBizEntityCoupon"]) 
        {
            message =[self translateCoupon:xmlData];
            
        }else if ([[doc.rootElement name] isEqualToString:@"ArrayOfBizEntityEvents"]) 
        {
            message =[self translateEvent:xmlData];
            
        }else if ([[doc.rootElement name] isEqualToString:@"ArrayOfBizEntityHealthTips"]) 
        {
            message =[self translateHealthTips:xmlData];
            
        }else if ([[doc.rootElement name] isEqualToString:@"ArrayOfBizEntityPromotion"]) 
        {
            message =[self translatePromotions:xmlData];
            
        }
        else if ([[doc.rootElement name] isEqualToString:@"ArrayOfBizEntityOutlet"]) 
        {
            message =[self translateOutlet:xmlData];
            
        }else if ([[doc.rootElement name] isEqualToString:@"ArrayOfBizEntityAppointment"]) 
        {
            message =[self translateAppointmentList:xmlData];
            
        }else if ([[doc.rootElement name] isEqualToString:@"ArrayOfBizEntitySaveAppointment"]) 
        {
            message =[self translateMakeAppointment:xmlData];
            
        }else if ([[doc.rootElement name] isEqualToString:@"ArrayOfBizEntityLogin"]) 
        {
            message =[self translateLogin:xmlData];
            
        }else if ([[doc.rootElement name] isEqualToString:@"ArrayOfBizEntityPoints"])
        {
            message =[self translateMyPoint:xmlData];
            
        }else if ([[doc.rootElement name] isEqualToString:@"ArrayOfBizEntityItem"])
        {
            message =[self translateAllPromotionsItem:xmlData];
            
        }else if ([[doc.rootElement name] isEqualToString:@"BizEntityOrder"])
        {
            message =[self translateReferenceNo:xmlData];
            NSLog(@"BizEntityOrder");
        }
        else if ([[doc.rootElement name] isEqualToString:@"ArrayOfBizEntityOrder"])
        {
            message =[self translateTransactions:xmlData];
            NSLog(@"ArrayOfBizEntityOrder");
        }
        [doc release];
    }
	else 
	{
		NSLog(@"%@: Error decoding the document: %@", [self class], [error localizedDescription]);
	}
    return message;
}

-(XMLResponse*)translateMyPoint:(NSData *)xmlData
{
    NSError *error;
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:xmlData options:0 error:&error];
    BpMyPointResponse *msg =nil;
    
    if (doc) { 
        NSArray *BizEntityCheckUp = [doc.rootElement elementsForName:@"BizEntityPoints"];
        if (BizEntityCheckUp.count>0) {
            
            msg =[[[BpMyPointResponse alloc] init]autorelease];
            NSMutableArray *BizEntity= [[NSMutableArray alloc]initWithCapacity:BizEntityCheckUp.count];
            
            for (GDataXMLElement *BizEntityElement in BizEntityCheckUp) {
                
                BpMyPoint *Object_item =[[BpMyPoint alloc]init];
                
                NSArray *resultCountDArr =[BizEntityElement elementsForName:@"resultCount"];                
                if (resultCountDArr.count >0) {
                    GDataXMLElement *resultCountDArrElement =(GDataXMLElement *)[resultCountDArr objectAtIndex:0];
                    Object_item.resultCount =[resultCountDArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *varUserIDArr =[BizEntityElement elementsForName:@"varUserID"];                
                if (varUserIDArr.count >0) {
                    GDataXMLElement *varUserIDArrElement =(GDataXMLElement *)[varUserIDArr objectAtIndex:0];
                    Object_item.varUserID =[varUserIDArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *PointEarnArr =[BizEntityElement elementsForName:@"PointEarn"];                
                if (PointEarnArr.count >0) {
                    GDataXMLElement *PointEarnArrElement =(GDataXMLElement *)[PointEarnArr objectAtIndex:0];
                    Object_item.PointEarn =[PointEarnArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *PointRebateArr =[BizEntityElement elementsForName:@"PointRebate"];                
                if (PointRebateArr.count >0) {
                    GDataXMLElement *PointRebateArrElement =(GDataXMLElement *)[PointRebateArr objectAtIndex:0];
                    Object_item.PointRebate =[PointRebateArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *PointBalanceArr =[BizEntityElement elementsForName:@"PointBalance"];                
                if (PointBalanceArr.count >0) {
                    GDataXMLElement *PointBalanceArrElement =(GDataXMLElement *)[PointBalanceArr objectAtIndex:0];
                    Object_item.PointBalance =[PointBalanceArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                [BizEntity addObject:Object_item];
                [Object_item release];
            }
            msg.MyPointArr=BizEntity;
            [BizEntity release];
        }
        [doc release];
    }
    return msg;
}

-(XMLResponse*)translateLogin:(NSData *)xmlData
{
    NSError *error;
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:xmlData options:0 error:&error];
    BploginResponse *msg =nil;
    
    if (doc) { 
        NSArray *BizEntityCheckUp = [doc.rootElement elementsForName:@"BizEntityLogin"];
        if (BizEntityCheckUp.count>0) {
            
            msg =[[[BploginResponse alloc] init]autorelease];
            NSMutableArray *BizEntity= [[NSMutableArray alloc]initWithCapacity:BizEntityCheckUp.count];
            
            for (GDataXMLElement *BizEntityElement in BizEntityCheckUp) {
                
                Bplogin *Object_item =[[Bplogin alloc]init];
                
                NSArray *resultCountDArr =[BizEntityElement elementsForName:@"resultCount"];                
                if (resultCountDArr.count >0) {
                    GDataXMLElement *resultCountDArrElement =(GDataXMLElement *)[resultCountDArr objectAtIndex:0];
                    Object_item.resultCount =[resultCountDArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }

                NSArray *LoginStatusArr =[BizEntityElement elementsForName:@"LoginStatus"];                
                if (LoginStatusArr.count >0) {
                    GDataXMLElement *LoginStatusArrElement =(GDataXMLElement *)[LoginStatusArr objectAtIndex:0];
                    Object_item.LoginStatus =[LoginStatusArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *ICNOArr =[BizEntityElement elementsForName:@"ICNO"];                
                if (ICNOArr.count >0) {
                    GDataXMLElement *ICNOArrElement =(GDataXMLElement *)[ICNOArr objectAtIndex:0];
                    Object_item.ICNO =[ICNOArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }

                
                [BizEntity addObject:Object_item];
                [Object_item release];
            }
            msg.loginArr=BizEntity;
            [BizEntity release];
        }
        [doc release];
    }
    return msg;

}

-(XMLResponse*)translateMakeAppointment:(NSData *)xmlData
{
    NSError *error;
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:xmlData options:0 error:&error];
    BpMakeAppointmentResponse *msg =nil;
    
    if (doc) { 
        NSArray *BizEntityCheckUp = [doc.rootElement elementsForName:@"BizEntitySaveAppointment"];
        if (BizEntityCheckUp.count>0) {
            
            msg =[[[BpMakeAppointmentResponse alloc] init]autorelease];
            NSMutableArray *BizEntity= [[NSMutableArray alloc]initWithCapacity:BizEntityCheckUp.count];
            
            for (GDataXMLElement *BizEntityElement in BizEntityCheckUp) {
                
                BpAppointment *Object_item =[[BpAppointment alloc]init];
                
                NSArray *insertResultArr =[BizEntityElement elementsForName:@"insertResult"];                
                if (insertResultArr.count >0) {
                    GDataXMLElement *insertResultArrElement =(GDataXMLElement *)[insertResultArr objectAtIndex:0];
                    Object_item.insertResult =[insertResultArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                
                
                [BizEntity addObject:Object_item];
                [Object_item release];
            }
            msg.AppointmentResponseArr=BizEntity;
            [BizEntity release];
        }
        [doc release];
    }
    return msg;

}

-(XMLResponse*)translateAppointmentList:(NSData *)xmlData
{
    NSError *error;
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:xmlData options:0 error:&error];
    BpAppointmentLsResponse *msg =nil;
    
    if (doc) { 
        NSArray *BizEntityCheckUp = [doc.rootElement elementsForName:@"BizEntityAppointment"];
        if (BizEntityCheckUp.count>0) {
            
            msg =[[[BpAppointmentLsResponse alloc] init]autorelease];
            NSMutableArray *BizEntity= [[NSMutableArray alloc]initWithCapacity:BizEntityCheckUp.count];
            
            for (GDataXMLElement *BizEntityElement in BizEntityCheckUp) {
                
                BpAppointmentList *Object_item =[[BpAppointmentList alloc]init];
                
                NSArray *resultCountDArr =[BizEntityElement elementsForName:@"resultCount"];                
                if (resultCountDArr.count >0) {
                    GDataXMLElement *resultCountDArrElement =(GDataXMLElement *)[resultCountDArr objectAtIndex:0];
                    Object_item.resultCount =[resultCountDArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *appointmentIDArr =[BizEntityElement elementsForName:@"appointmentID"];                
                if (appointmentIDArr.count >0) {
                    GDataXMLElement *appointmentIDArrElement =(GDataXMLElement *)[appointmentIDArr objectAtIndex:0];
                    Object_item.appointmentID =[appointmentIDArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *appointmentServiceRequestArr =[BizEntityElement elementsForName:@"appointmentServiceRequest"];                
                if (appointmentServiceRequestArr.count >0) {
                    GDataXMLElement *appointmentServiceRequestArrElement =(GDataXMLElement *)[appointmentServiceRequestArr objectAtIndex:0];
                    Object_item.appointmentServiceRequest =[appointmentServiceRequestArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                NSArray *appointmentSortArr =[BizEntityElement elementsForName:@"appointmentSort"];                
                if (appointmentSortArr.count >0) {
                    GDataXMLElement *appointmentSortArrElement =(GDataXMLElement *)[appointmentSortArr objectAtIndex:0];
                    Object_item.appointmentSort =[appointmentSortArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                NSArray *appointmentStatusArr =[BizEntityElement elementsForName:@"appointmentStatus"];                
                if (appointmentStatusArr.count >0) {
                    GDataXMLElement *appointmentStatusArrElement =(GDataXMLElement *)[appointmentStatusArr objectAtIndex:0];
                    Object_item.appointmentStatus =[appointmentStatusArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                NSArray *appointmentNoOfPersonArr =[BizEntityElement elementsForName:@"appointmentNoOfPerson"];                
                if (appointmentNoOfPersonArr.count >0) {
                    GDataXMLElement *appointmentNoOfPersonArrElement =(GDataXMLElement *)[appointmentNoOfPersonArr objectAtIndex:0];
                    Object_item.appointmentNoOfPerson =[appointmentNoOfPersonArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                NSArray *appointmentCreateDateArr =[BizEntityElement elementsForName:@"appointmentCreateDate"];                
                if (appointmentCreateDateArr.count >0) {
                    GDataXMLElement *appointmentCreateDateArrElement =(GDataXMLElement *)[appointmentCreateDateArr objectAtIndex:0];
                    Object_item.appointmentCreateDate =[appointmentCreateDateArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                NSArray *appointmentBookDateTimeArr =[BizEntityElement elementsForName:@"appointmentBookDateTime"];                
                if (appointmentBookDateTimeArr.count >0) {
                    GDataXMLElement *appointmentBookDateTimeArrElement =(GDataXMLElement *)[appointmentBookDateTimeArr objectAtIndex:0];
                    Object_item.appointmentBookDateTime =[appointmentBookDateTimeArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                NSArray *appointmentBranchArr =[BizEntityElement elementsForName:@"appointmentBranch"];                
                if (appointmentBranchArr.count >0) {
                    GDataXMLElement *appointmentBranchArrElement =(GDataXMLElement *)[appointmentBranchArr objectAtIndex:0];
                    Object_item.appointmentBranch =[appointmentBranchArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                NSArray *userIDArr =[BizEntityElement elementsForName:@"userID"];                
                if (userIDArr.count >0) {
                    GDataXMLElement *userIDArrElement =(GDataXMLElement *)[userIDArr objectAtIndex:0];
                    Object_item.userID =[userIDArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                NSArray *tokenIDArr =[BizEntityElement elementsForName:@"tokenID"];                
                if (tokenIDArr.count >0) {
                    GDataXMLElement *tokenIDArrElement =(GDataXMLElement *)[tokenIDArr objectAtIndex:0];
                    Object_item.tokenID =[tokenIDArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                NSArray *adminRemarkArr =[BizEntityElement elementsForName:@"adminRemark"];                
                if (adminRemarkArr.count >0) {
                    GDataXMLElement *adminRemarkArrElement =(GDataXMLElement *)[adminRemarkArr objectAtIndex:0];
                    Object_item.adminRemark =[adminRemarkArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                NSArray *customerNameArr =[BizEntityElement elementsForName:@"customerName"];                
                if (customerNameArr.count >0) {
                    GDataXMLElement *customerNameArrElement =(GDataXMLElement *)[customerNameArr objectAtIndex:0];
                    Object_item.customerName =[customerNameArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                NSArray *customerContactArr =[BizEntityElement elementsForName:@"customerContact"];                
                if (customerContactArr.count >0) {
                    GDataXMLElement *customerContactArrElement =(GDataXMLElement *)[customerContactArr objectAtIndex:0];
                    Object_item.customerContact =[customerContactArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                NSArray *customerEmailArr =[BizEntityElement elementsForName:@"customerEmail"];                
                if (customerEmailArr.count >0) {
                    GDataXMLElement *customerEmailArrElement =(GDataXMLElement *)[customerEmailArr objectAtIndex:0];
                    Object_item.customerEmail =[customerEmailArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                NSArray *customerRemarkArr =[BizEntityElement elementsForName:@"customerRemark"];                
                if (customerRemarkArr.count >0) {
                    GDataXMLElement *customerRemarkArrElement =(GDataXMLElement *)[customerRemarkArr objectAtIndex:0];
                    Object_item.customerRemark =[customerRemarkArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                NSArray *insertResultArr =[BizEntityElement elementsForName:@"insertResult"];                
                if (insertResultArr.count >0) {
                    GDataXMLElement *insertResultArrElement =(GDataXMLElement *)[insertResultArr objectAtIndex:0];
                    Object_item.insertResult =[insertResultArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                
       
                
                
                
                [BizEntity addObject:Object_item];
                [Object_item release];
            }
            msg.AppointmentLsArr=BizEntity;
            [BizEntity release];
        }
        [doc release];
    }
    return msg;

}

-(XMLResponse*)translateOutlet:(NSData *)xmlData
{
    NSError *error;
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:xmlData options:0 error:&error];
    BpOutletResponse *msg =nil;
    
    if (doc) { 
        NSArray *BizEntityCheckUp = [doc.rootElement elementsForName:@"BizEntityOutlet"];
        if (BizEntityCheckUp.count>0) {
            
            msg =[[[BpOutletResponse alloc] init]autorelease];
            NSMutableArray *BizEntity= [[NSMutableArray alloc]initWithCapacity:BizEntityCheckUp.count];
            
            for (GDataXMLElement *BizEntityElement in BizEntityCheckUp) {
                
                BpOutlet *Object_item =[[BpOutlet alloc]init];
                
                NSArray *resultCountDArr =[BizEntityElement elementsForName:@"resultCount"];                
                if (resultCountDArr.count >0) {
                    GDataXMLElement *resultCountDArrElement =(GDataXMLElement *)[resultCountDArr objectAtIndex:0];
                    Object_item.resultCount =[resultCountDArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *RowIDArr =[BizEntityElement elementsForName:@"RowID"];                
                if (RowIDArr.count >0) {
                    GDataXMLElement *RowIDArrElement =(GDataXMLElement *)[RowIDArr objectAtIndex:0];
                    Object_item.RowID =[RowIDArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *BranchNameArr =[BizEntityElement elementsForName:@"BranchName"];                
                if (BranchNameArr.count >0) {
                    GDataXMLElement *BranchNameArrElement =(GDataXMLElement *)[BranchNameArr objectAtIndex:0];
                    Object_item.BranchName =[BranchNameArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *Address_1Arr =[BizEntityElement elementsForName:@"Address_1"];                
                if (Address_1Arr.count >0) {
                    GDataXMLElement *Address_1ArrElement =(GDataXMLElement *)[Address_1Arr objectAtIndex:0];
                    Object_item.Address_1 =[Address_1ArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *Address_2Arr =[BizEntityElement elementsForName:@"Address_2"];                
                if (Address_2Arr.count >0) {
                    GDataXMLElement *Address_2ArrElement =(GDataXMLElement *)[Address_2Arr objectAtIndex:0];
                    Object_item.Address_2 =[Address_2ArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *Address_3Arr =[BizEntityElement elementsForName:@"Address_3"];                
                if (Address_3Arr.count >0) {
                    GDataXMLElement *Address_3ArrElement =(GDataXMLElement *)[Address_3Arr objectAtIndex:0];
                    Object_item.Address_3 =[Address_3ArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *TelArr =[BizEntityElement elementsForName:@"Tel"];                
                if (TelArr.count >0) {
                    GDataXMLElement *TelArrElement =(GDataXMLElement *)[TelArr objectAtIndex:0];
                    Object_item.Tel =[TelArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *FaxArr =[BizEntityElement elementsForName:@"Fax"];                
                if (FaxArr.count >0) {
                    GDataXMLElement *FaxArrElement =(GDataXMLElement *)[FaxArr objectAtIndex:0];
                    Object_item.Fax =[FaxArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *LocationIDArr =[BizEntityElement elementsForName:@"LocationID"];                
                if (LocationIDArr.count >0) {
                    GDataXMLElement *LocationIDArrElement =(GDataXMLElement *)[LocationIDArr objectAtIndex:0];
                    Object_item.LocationID =[LocationIDArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *BranchImgArr =[BizEntityElement elementsForName:@"BranchImg"];                
                if (BranchImgArr.count >0) {
                    GDataXMLElement *BranchImgArrElement =(GDataXMLElement *)[BranchImgArr objectAtIndex:0];
                    Object_item.BranchImg =[BranchImgArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                NSArray *LocationImgArr =[BizEntityElement elementsForName:@"LocationImg"];                
                if (LocationImgArr.count >0) {
                    GDataXMLElement *LocationImgArrElement =(GDataXMLElement *)[LocationImgArr objectAtIndex:0];
                    Object_item.LocationImg =[LocationImgArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                NSArray *BusinessHourImgArr =[BizEntityElement elementsForName:@"BusinessHourImg"];                
                if (BusinessHourImgArr.count >0) {
                    GDataXMLElement *BusinessHourImgArrElement =(GDataXMLElement *)[BusinessHourImgArr objectAtIndex:0];
                    Object_item.BusinessHourImg =[BusinessHourImgArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                NSArray *TypeDCArr =[BizEntityElement elementsForName:@"TypeDC"];                
                if (TypeDCArr.count >0) {
                    GDataXMLElement *TypeDCArrElement =(GDataXMLElement *)[TypeDCArr objectAtIndex:0];
                    Object_item.TypeDC =[TypeDCArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                NSArray *TypeLABArr =[BizEntityElement elementsForName:@"TypeLAB"];                
                if (TypeLABArr.count >0) {
                    GDataXMLElement *TypeLABArrElement =(GDataXMLElement *)[TypeLABArr objectAtIndex:0];
                    Object_item.TypeLAB =[TypeLABArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                NSArray *TypeHEARINGArr =[BizEntityElement elementsForName:@"TypeHEARING"];                
                if (TypeHEARINGArr.count >0) {
                    GDataXMLElement *TypeHEARINGArrElement =(GDataXMLElement *)[TypeHEARINGArr objectAtIndex:0];
                    Object_item.TypeHEARING =[TypeHEARINGArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                NSArray *TypeDISPENSARYArr =[BizEntityElement elementsForName:@"TypeDISPENSARY"];                
                if (TypeDISPENSARYArr.count >0) {
                    GDataXMLElement *TypeDISPENSARYArrElement =(GDataXMLElement *)[TypeDISPENSARYArr objectAtIndex:0];
                    Object_item.TypeDISPENSARY =[TypeDISPENSARYArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                NSArray *MapURLArr =[BizEntityElement elementsForName:@"MapURL"];                
                if (MapURLArr.count >0) {
                    GDataXMLElement *MapURLArrElement =(GDataXMLElement *)[MapURLArr objectAtIndex:0];
                    Object_item.MapURL =[MapURLArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                NSArray *LatitudeArr =[BizEntityElement elementsForName:@"Latitude"];                
                if (LatitudeArr.count >0) {
                    GDataXMLElement *LatitudeArrElement =(GDataXMLElement *)[LatitudeArr objectAtIndex:0];
                    Object_item.Latitude =[LatitudeArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }

                NSArray *LongitudeArr =[BizEntityElement elementsForName:@"Longitude"];                
                if (LongitudeArr.count >0) {
                    GDataXMLElement *LongitudeArrElement =(GDataXMLElement *)[LongitudeArr objectAtIndex:0];
                    Object_item.Longitude =[LongitudeArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                NSArray *VisibilityArr =[BizEntityElement elementsForName:@"Visibility"];                
                if (VisibilityArr.count >0) {
                    GDataXMLElement *VisibilityArrElement =(GDataXMLElement *)[VisibilityArr objectAtIndex:0];
                    Object_item.Visibility =[VisibilityArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                
                [BizEntity addObject:Object_item];
                [Object_item release];
            }
            msg.OutletArr=BizEntity;
            [BizEntity release];
        }
        [doc release];
    }
    return msg;
}

-(XMLResponse*)translatePromotions:(NSData *)xmlData
{
    NSError *error;
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:xmlData options:0 error:&error];
    BpPromotionsResponse *msg =nil;
    
    if (doc) {
        NSArray *BizEntityCheckUp = [doc.rootElement elementsForName:@"BizEntityPromotion"];
        if (BizEntityCheckUp.count>0) {
            
            msg =[[[BpPromotionsResponse alloc] init]autorelease];
            NSMutableArray *BizEntity= [[NSMutableArray alloc]initWithCapacity:BizEntityCheckUp.count];
            
            for (GDataXMLElement *BizEntityElement in BizEntityCheckUp) {
                
                BpPromotions *Object_item =[[BpPromotions alloc]init];
                
                NSArray *resultCountDArr =[BizEntityElement elementsForName:@"resultCount"];
                if (resultCountDArr.count >0) {
                    GDataXMLElement *resultCountDArrElement =(GDataXMLElement *)[resultCountDArr objectAtIndex:0];
                    Object_item.resultCount =[resultCountDArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *promotionIDArr =[BizEntityElement elementsForName:@"promotionID"];
                if (promotionIDArr.count >0) {
                    GDataXMLElement *promotionIDArrElement =(GDataXMLElement *)[promotionIDArr objectAtIndex:0];
                    Object_item.promotionID =[promotionIDArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *promotionTitleArr =[BizEntityElement elementsForName:@"promotionTitle"];
                if (promotionTitleArr.count >0) {
                    GDataXMLElement *promotionTitleArrElement =(GDataXMLElement *)[promotionTitleArr objectAtIndex:0];
                    Object_item.promotionTitle =[promotionTitleArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *promotionDescpArr =[BizEntityElement elementsForName:@"promotionDescp"];
                if (promotionDescpArr.count >0) {
                    GDataXMLElement *promotionDescpArrElement =(GDataXMLElement *)[promotionDescpArr objectAtIndex:0];
                    Object_item.promotionDescp =[promotionDescpArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *promotionSortArr =[BizEntityElement elementsForName:@"promotionSort"];
                if (promotionSortArr.count >0) {
                    GDataXMLElement *promotionSortArrElement =(GDataXMLElement *)[promotionSortArr objectAtIndex:0];
                    Object_item.promotionSort =[promotionSortArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *promotionStatusArr =[BizEntityElement elementsForName:@"promotionStatus"];
                if (promotionStatusArr.count >0) {
                    GDataXMLElement *promotionStatusArrElement =(GDataXMLElement *)[promotionStatusArr objectAtIndex:0];
                    Object_item.promotionStatus =[promotionStatusArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *promotionDateTimeArr =[BizEntityElement elementsForName:@"promotionDateTime"];
                if (promotionDateTimeArr.count >0) {
                    GDataXMLElement *promotionDateTimeArrElement =(GDataXMLElement *)[promotionDateTimeArr objectAtIndex:0];
                    Object_item.promotionDateTime =[promotionDateTimeArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *promotionImgPathArr =[BizEntityElement elementsForName:@"promotionImgPath"];
                if (promotionImgPathArr.count >0) {
                    GDataXMLElement *promotionImgPathArrElement =(GDataXMLElement *)[promotionImgPathArr objectAtIndex:0];
                    Object_item.promotionImgPath =[promotionImgPathArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                
                
                [BizEntity addObject:Object_item];
                [Object_item release];
            }
            msg.PromotionsArr=BizEntity;
            [BizEntity release];
        }
        [doc release];
    }
    return msg;
}

//Promotion items
-(XMLResponse*)translateAllPromotionsItem:(NSData *)xmlData
{
    NSError *error;
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:xmlData options:0 error:&error];
    BpGetAllItemsResponse *msg =nil;
    
    if (doc) {
        NSArray *BizEntityCheckUp = [doc.rootElement elementsForName:@"BizEntityItem"];
        
        if (BizEntityCheckUp.count>0) {
            
            msg =[[[BpGetAllItemsResponse alloc] init]autorelease];
            NSMutableArray *BizEntity= [[NSMutableArray alloc]initWithCapacity:BizEntityCheckUp.count];
            
            for (GDataXMLElement *BizEntityElement in BizEntityCheckUp) {
                NSLog(@"root element : %@", [BizEntityCheckUp objectAtIndex:0]);
                BpPromotionItem *Object_item =[[BpPromotionItem alloc]init];
                
                NSArray *resultCountDArr =[BizEntityElement elementsForName:@"Name"];
                if (resultCountDArr.count >0) {
                    GDataXMLElement *resultCountDArrElement =(GDataXMLElement *)[resultCountDArr objectAtIndex:0];
                    Object_item.Name =[resultCountDArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *promotionIDArr =[BizEntityElement elementsForName:@"CreateDate"];
                if (promotionIDArr.count >0) {
                    GDataXMLElement *promotionIDArrElement =(GDataXMLElement *)[promotionIDArr objectAtIndex:0];
                    Object_item.CreateDate =[promotionIDArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *promotionTitleArr =[BizEntityElement elementsForName:@"EditDate"];
                if (promotionTitleArr.count >0) {
                    GDataXMLElement *promotionTitleArrElement =(GDataXMLElement *)[promotionTitleArr objectAtIndex:0];
                    Object_item.EditDate =[promotionTitleArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *promotionDescpArr =[BizEntityElement elementsForName:@"Descp"];
                if (promotionDescpArr.count >0) {
                    GDataXMLElement *promotionDescpArrElement =(GDataXMLElement *)[promotionDescpArr objectAtIndex:0];
                    Object_item.Descp =[promotionDescpArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *promotionSortArr =[BizEntityElement elementsForName:@"DiscountPrice"];
                if (promotionSortArr.count >0) {
                    GDataXMLElement *promotionSortArrElement =(GDataXMLElement *)[promotionSortArr objectAtIndex:0];
                    Object_item.DiscountPrice =[promotionSortArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *promotionStatusArr =[BizEntityElement elementsForName:@"NormalPrice"];
                if (promotionStatusArr.count >0) {
                    GDataXMLElement *promotionStatusArrElement =(GDataXMLElement *)[promotionStatusArr objectAtIndex:0];
                    Object_item.NormalPrice =[promotionStatusArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *promotionDateTimeArr =[BizEntityElement elementsForName:@"PicURl"];
                if (promotionDateTimeArr.count >0) {
                    GDataXMLElement *promotionDateTimeArrElement =(GDataXMLElement *)[promotionDateTimeArr objectAtIndex:0];
                    Object_item.PicURl =[promotionDateTimeArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
               
                [BizEntity addObject:Object_item];
                [Object_item release];
            }
            msg.PromotionsArr=BizEntity;
            [BizEntity release];
        }
        [doc release];
    }
    return msg;
}


-(XMLResponse*)translateHealthTips:(NSData *)xmlData
{
    NSError *error;
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:xmlData options:0 error:&error];
    BpHealthTipsResponse *msg =nil;
    
    if (doc) {
        NSArray *BizEntityCheckUp = [doc.rootElement elementsForName:@"BizEntityHealthTips"];
        if (BizEntityCheckUp.count>0) {
            
            msg =[[[BpHealthTipsResponse alloc] init]autorelease];
            NSMutableArray *BizEntity= [[NSMutableArray alloc]initWithCapacity:BizEntityCheckUp.count];
            
            for (GDataXMLElement *BizEntityElement in BizEntityCheckUp) {
                
                BpHealthTips *Object_item =[[BpHealthTips alloc]init];
                
                NSArray *resultCountDArr =[BizEntityElement elementsForName:@"resultCount"];                
                if (resultCountDArr.count >0) {
                    GDataXMLElement *resultCountDArrElement =(GDataXMLElement *)[resultCountDArr objectAtIndex:0];
                    Object_item.resultCount =[resultCountDArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *healthtipsIDArr =[BizEntityElement elementsForName:@"healthtipsID"];                
                if (healthtipsIDArr.count >0) {
                    GDataXMLElement *healthtipsIDArrElement =(GDataXMLElement *)[healthtipsIDArr objectAtIndex:0];
                    Object_item.healthtipsID =[healthtipsIDArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }

                NSArray *healthtipsTitleArr =[BizEntityElement elementsForName:@"healthtipsTitle"];                
                if (healthtipsTitleArr.count >0) {
                    GDataXMLElement *healthtipsTitleArrElement =(GDataXMLElement *)[healthtipsTitleArr objectAtIndex:0];
                    Object_item.healthtipsTitle =[healthtipsTitleArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *healthtipsDescpArr =[BizEntityElement elementsForName:@"healthtipsDescp"];                
                if (healthtipsDescpArr.count >0) {
                    GDataXMLElement *healthtipsDescpArrElement =(GDataXMLElement *)[healthtipsDescpArr objectAtIndex:0];
                    Object_item.healthtipsDescp =[healthtipsDescpArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *healthtipsSortArr =[BizEntityElement elementsForName:@"healthtipsSort"];                
                if (healthtipsSortArr.count >0) {
                    GDataXMLElement *healthtipsSortArrElement =(GDataXMLElement *)[healthtipsSortArr objectAtIndex:0];
                    Object_item.healthtipsSort =[healthtipsSortArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *healthtipsStatusArr =[BizEntityElement elementsForName:@"healthtipsStatus"];                
                if (healthtipsStatusArr.count >0) {
                    GDataXMLElement *healthtipsStatusArrElement =(GDataXMLElement *)[healthtipsStatusArr objectAtIndex:0];
                    Object_item.healthtipsStatus =[healthtipsStatusArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *healthtipsDateTimeArr =[BizEntityElement elementsForName:@"healthtipsDateTime"];                
                if (healthtipsDateTimeArr.count >0) {
                    GDataXMLElement *healthtipsDateTimeArrElement =(GDataXMLElement *)[healthtipsDateTimeArr objectAtIndex:0];
                    Object_item.healthtipsDateTime =[healthtipsDateTimeArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *healthtipsImgPathArr =[BizEntityElement elementsForName:@"healthtipsImgPath"];                
                if (healthtipsImgPathArr.count >0) {
                    GDataXMLElement *healthtipsImgPathArrElement =(GDataXMLElement *)[healthtipsImgPathArr objectAtIndex:0];
                    Object_item.healthtipsImgPath =[healthtipsImgPathArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }


                
                [BizEntity addObject:Object_item];
                [Object_item release];
            }
            msg.HealthTipsArr=BizEntity;
            [BizEntity release];
        }
        [doc release];
    }
    return msg;
}

-(XMLResponse*)translateEvent:(NSData *)xmlData
{
    NSError *error;
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:xmlData options:0 error:&error];
    BpEventResponse *msg =nil;
    
    if (doc) {
        NSArray *BizEntityCheckUp = [doc.rootElement elementsForName:@"BizEntityEvents"];
        if (BizEntityCheckUp.count>0) {
            
            msg =[[[BpEventResponse alloc] init]autorelease];
            NSMutableArray *BizEntity= [[NSMutableArray alloc]initWithCapacity:BizEntityCheckUp.count];
            
            for (GDataXMLElement *BizEntityElement in BizEntityCheckUp) {
                
                BpEvent *Object_item =[[BpEvent alloc]init];
                
                NSArray *resultCountDArr =[BizEntityElement elementsForName:@"resultCount"];                
                if (resultCountDArr.count >0) {
                    GDataXMLElement *resultCountDArrElement =(GDataXMLElement *)[resultCountDArr objectAtIndex:0];
                    Object_item.resultCount =[resultCountDArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *eventsIDArr =[BizEntityElement elementsForName:@"eventsID"];                
                if (eventsIDArr.count >0) {
                    GDataXMLElement *eventsIDArrElement =(GDataXMLElement *)[eventsIDArr objectAtIndex:0];
                    Object_item.eventsID =[eventsIDArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *eventsTitleArr =[BizEntityElement elementsForName:@"eventsTitle"];                
                if (eventsTitleArr.count >0) {
                    GDataXMLElement *eventsTitleArrElement =(GDataXMLElement *)[eventsTitleArr objectAtIndex:0];
                    Object_item.eventsTitle =[eventsTitleArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *eventsDescpArr =[BizEntityElement elementsForName:@"eventsDescp"];                
                if (eventsDescpArr.count >0) {
                    GDataXMLElement *eventsDescpArrElement =(GDataXMLElement *)[eventsDescpArr objectAtIndex:0];
                    Object_item.eventsDescp =[eventsDescpArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *eventsSortArr =[BizEntityElement elementsForName:@"eventsSort"];                
                if (eventsSortArr.count >0) {
                    GDataXMLElement *eventsSortArrElement =(GDataXMLElement *)[eventsSortArr objectAtIndex:0];
                    Object_item.eventsSort =[eventsSortArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *eventsStatusArr =[BizEntityElement elementsForName:@"eventsStatus"];                
                if (eventsStatusArr.count >0) {
                    GDataXMLElement *eventsStatusArrElement =(GDataXMLElement *)[eventsStatusArr objectAtIndex:0];
                    Object_item.eventsStatus =[eventsStatusArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *eventsDateTimeArr =[BizEntityElement elementsForName:@"eventsDateTime"];                
                if (eventsDateTimeArr.count >0) {
                    GDataXMLElement *eventsDateTimeArrElement =(GDataXMLElement *)[eventsDateTimeArr objectAtIndex:0];
                    Object_item.eventsDateTime =[eventsDateTimeArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }

                NSArray *eventsImgPathArr =[BizEntityElement elementsForName:@"eventsImgPath"];                
                if (eventsImgPathArr.count >0) {
                    GDataXMLElement *eventsImgPathArrElement =(GDataXMLElement *)[eventsImgPathArr objectAtIndex:0];
                    Object_item.eventsImgPath =[eventsImgPathArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }

                NSArray *eventsCreateDateArr =[BizEntityElement elementsForName:@"eventsCreateDate"];                
                if (eventsCreateDateArr.count >0) {
                    GDataXMLElement *eventsCreateDateArrElement =(GDataXMLElement *)[eventsCreateDateArr objectAtIndex:0];
                    Object_item.eventsCreateDate =[eventsCreateDateArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }

                
                [BizEntity addObject:Object_item];
                [Object_item release];
            }
            msg.EventArr=BizEntity;
            [BizEntity release];
        }
        [doc release];
    }
    return msg;

}

-(XMLResponse*)translateCoupon:(NSData *)xmlData
{
    NSError *error;
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:xmlData options:0 error:&error];
    BpCouponResponse *msg =nil;
    
    if (doc) {
        NSArray *BizEntityCheckUp = [doc.rootElement elementsForName:@"BizEntityCoupon"];
        if (BizEntityCheckUp.count>0) {
            
            msg =[[[BpCouponResponse alloc] init]autorelease];
            NSMutableArray *BizEntity= [[NSMutableArray alloc]initWithCapacity:BizEntityCheckUp.count];
            
            for (GDataXMLElement *BizEntityElement in BizEntityCheckUp) {
                
                BpCoupon *Object_item =[[BpCoupon alloc]init];

                NSArray *resultCountDArr =[BizEntityElement elementsForName:@"resultCount"];                
                if (resultCountDArr.count >0) {
                    GDataXMLElement *resultCountDArrElement =(GDataXMLElement *)[resultCountDArr objectAtIndex:0];
                    Object_item.resultCount =[resultCountDArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *couponIDArr =[BizEntityElement elementsForName:@"couponID"];                
                if (couponIDArr.count >0) {
                    GDataXMLElement *couponIDArrElement =(GDataXMLElement *)[couponIDArr objectAtIndex:0];
                    Object_item.couponID =[couponIDArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *couponTitleArr =[BizEntityElement elementsForName:@"couponTitle"];                
                if (couponTitleArr.count >0) {
                    GDataXMLElement *couponTitleArrElement =(GDataXMLElement *)[couponTitleArr objectAtIndex:0];
                    Object_item.couponTitle =[couponTitleArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *couponDescpArr =[BizEntityElement elementsForName:@"couponDescp"];                
                if (couponDescpArr.count >0) {
                    GDataXMLElement *couponDescpArrElement =(GDataXMLElement *)[couponDescpArr objectAtIndex:0];
                    Object_item.couponDescp =[couponDescpArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *couponSortArr =[BizEntityElement elementsForName:@"couponSort"];                
                if (couponSortArr.count >0) {
                    GDataXMLElement *couponSortArrElement =(GDataXMLElement *)[couponSortArr objectAtIndex:0];
                    Object_item.couponSort =[couponSortArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *couponStatusArr =[BizEntityElement elementsForName:@"couponStatus"];                
                if (couponStatusArr.count >0) {
                    GDataXMLElement *couponStatusArrElement =(GDataXMLElement *)[couponStatusArr objectAtIndex:0];
                    Object_item.couponStatus =[couponStatusArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *couponDateTimeArr =[BizEntityElement elementsForName:@"couponDateTime"];                
                if (couponDateTimeArr.count >0) {
                    GDataXMLElement *couponDateTimeArrElement =(GDataXMLElement *)[couponDateTimeArr objectAtIndex:0];
                    Object_item.couponDateTime =[couponDateTimeArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *couponImgPathArr =[BizEntityElement elementsForName:@"couponImgPath"];                
                if (couponImgPathArr.count >0) {
                    GDataXMLElement *couponImgPathArrElement =(GDataXMLElement *)[couponImgPathArr objectAtIndex:0];
                    Object_item.couponImgPath =[couponImgPathArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                
                
                [BizEntity addObject:Object_item];
                [Object_item release];
            }
            msg.CouponArr=BizEntity;
            [BizEntity release];
        }
        [doc release];
    }
    return msg;
}

-(XMLResponse*)translateFestival:(NSData *)xmlData
{
    NSError *error;
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:xmlData options:0 error:&error];
    BpFestivalResponse *msg =nil;
    
    if (doc) {
        NSArray *BizEntityCheckUp = [doc.rootElement elementsForName:@"BizEntityFestival"];
        if (BizEntityCheckUp.count>0) {
            
            msg =[[[BpFestivalResponse alloc] init]autorelease];
            NSMutableArray *BizEntity= [[NSMutableArray alloc]initWithCapacity:BizEntityCheckUp.count];
            
            for (GDataXMLElement *BizEntityElement in BizEntityCheckUp) {
                
                BpFestival *Object_item =[[BpFestival alloc]init];
                
                NSArray *resultCountDArr =[BizEntityElement elementsForName:@"resultCount"];                
                if (resultCountDArr.count >0) {
                    GDataXMLElement *resultCountDArrElement =(GDataXMLElement *)[resultCountDArr objectAtIndex:0];
                    Object_item.resultCount =[resultCountDArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                }
                
                NSArray *festivalIDArr =[BizEntityElement elementsForName:@"festivalID"];
                if (festivalIDArr.count >0) {
                    GDataXMLElement *festivalIDArrElement =(GDataXMLElement *)[festivalIDArr objectAtIndex:0];
                    Object_item.festivalID =[festivalIDArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                NSArray *festivalTitleArr =[BizEntityElement elementsForName:@"festivalTitle"];
                if (festivalTitleArr.count >0) {
                    GDataXMLElement *festivalTitleArrElement =(GDataXMLElement *)[festivalTitleArr objectAtIndex:0];
                    Object_item.festivalTitle =[festivalTitleArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                NSArray *festivalDescpArr =[BizEntityElement elementsForName:@"festivalDescp"];
                if (festivalDescpArr.count >0) {
                    GDataXMLElement *festivalDescpArrElement =(GDataXMLElement *)[festivalDescpArr objectAtIndex:0];
                    Object_item.festivalDescp =[festivalDescpArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                NSArray *festivalSortArr =[BizEntityElement elementsForName:@"festivalSort"];
                if (festivalSortArr.count >0) {
                    GDataXMLElement *festivalSortArrElement =(GDataXMLElement *)[festivalSortArr objectAtIndex:0];
                    Object_item.festivalSort =[festivalSortArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                NSArray *festivalStatusArr =[BizEntityElement elementsForName:@"festivalStatus"];
                if (festivalStatusArr.count >0) {
                    GDataXMLElement *festivalStatusArrElement =(GDataXMLElement *)[festivalStatusArr objectAtIndex:0];
                    Object_item.festivalStatus =[festivalStatusArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }

                NSArray *festivalCreateDateArr =[BizEntityElement elementsForName:@"festivalCreateDate"];
                if (festivalCreateDateArr.count >0) {
                    GDataXMLElement *festivalCreateDateArrElement =(GDataXMLElement *)[festivalCreateDateArr objectAtIndex:0];
                    Object_item.festivalCreateDate =[festivalCreateDateArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                NSArray *festivalImgPathArr =[BizEntityElement elementsForName:@"festivalImgPath"];
                if (festivalImgPathArr.count >0) {
                    GDataXMLElement *festivalImgPathArrElement =(GDataXMLElement *)[festivalImgPathArr objectAtIndex:0];
                    Object_item.festivalImgPath =[festivalImgPathArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }

                
                [BizEntity addObject:Object_item];
                [Object_item release];
            }
            msg.FestivalArr=BizEntity;
            [BizEntity release];
        }
        [doc release];
    }
    return msg;
}
 
-(XMLResponse*)translateCheckUp:(NSData *)xmlData
{
    NSError *error;
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:xmlData options:0 error:&error];
    BpCheckUpResponse *msg =nil;
    
    if (doc) {
        NSArray *BizEntityCheckUp = [doc.rootElement elementsForName:@"BizEntityCheckUp"];
        if (BizEntityCheckUp.count>0) {
            
            msg =[[[BpCheckUpResponse alloc] init]autorelease];
            NSMutableArray *BizEntity= [[NSMutableArray alloc]initWithCapacity:BizEntityCheckUp.count];
            
            for (GDataXMLElement *BizEntityElement in BizEntityCheckUp) {
                
                BpCheckUp *CgetDiscoverBali =[[BpCheckUp alloc]init];
                
                NSArray *DiscoverBaliImageIDArr =[BizEntityElement elementsForName:@"resultCount"];                
                if (DiscoverBaliImageIDArr.count >0) {
                    GDataXMLElement *DiscoverBaliImageIDElement =(GDataXMLElement *)[DiscoverBaliImageIDArr objectAtIndex:0];
                    CgetDiscoverBali.resultCount =[DiscoverBaliImageIDElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

                }
                
                NSArray *ImagePathArr =[BizEntityElement elementsForName:@"checkupID"];
                if (ImagePathArr.count >0) {
                    GDataXMLElement *ImagePathElement =(GDataXMLElement *)[ImagePathArr objectAtIndex:0];
                    CgetDiscoverBali.checkupID =[ImagePathElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                NSArray *ImageDescriptionArr =[BizEntityElement elementsForName:@"checkupTitle"];
                if (ImageDescriptionArr.count >0) {
                    GDataXMLElement *ImageDescriptionElement =(GDataXMLElement *)[ImageDescriptionArr objectAtIndex:0];
                    CgetDiscoverBali.checkupTitle =[ImageDescriptionElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                NSArray *checkupDescpArr =[BizEntityElement elementsForName:@"checkupDescp"];
                if (checkupDescpArr.count >0) {
                    GDataXMLElement *checkupDescpArrElement =(GDataXMLElement *)[checkupDescpArr objectAtIndex:0];
                    CgetDiscoverBali.checkupDescp =[checkupDescpArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                NSArray *checkupSortArr =[BizEntityElement elementsForName:@"checkupSort"];
                if (checkupSortArr.count >0) {
                    GDataXMLElement *checkupSortArrElement =(GDataXMLElement *)[checkupSortArr objectAtIndex:0];
                    CgetDiscoverBali.checkupSort =[checkupSortArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                NSArray *checkupStatusArr =[BizEntityElement elementsForName:@"checkupStatus"];
                if (checkupStatusArr.count >0) {
                    GDataXMLElement *checkupStatusArrElement =(GDataXMLElement *)[checkupStatusArr objectAtIndex:0];
                    CgetDiscoverBali.checkupStatus =[checkupStatusArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                NSArray *checkupDateTimeArr =[BizEntityElement elementsForName:@"checkupDateTime"];
                if (checkupDateTimeArr.count >0) {
                    GDataXMLElement *checkupDateTimeArrElement =(GDataXMLElement *)[checkupDateTimeArr objectAtIndex:0];
                    CgetDiscoverBali.checkupDateTime =[checkupDateTimeArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                NSArray *checkupImgPathArr =[BizEntityElement elementsForName:@"checkupImgPath"];
                if (checkupImgPathArr.count >0) {
                    GDataXMLElement *checkupImgPathArrElement =(GDataXMLElement *)[checkupImgPathArr objectAtIndex:0];
                    CgetDiscoverBali.checkupImgPath =[checkupImgPathArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                
                [BizEntity addObject:CgetDiscoverBali];
                [CgetDiscoverBali release];
            }
            msg.CheckUpArr=BizEntity;
            [BizEntity release];
        }
        [doc release];
    }
    return msg;

}

-(XMLResponse*)translateBirthday:(NSData *)xmlData
{
    NSError *error;
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:xmlData options:0 error:&error];
    BpBirthdayResponse *msg =nil;
    
    if (doc) {
        NSArray *BizEntityDiscoverBali = [doc.rootElement elementsForName:@"BizEntityBirthdayTreatImage"];
        if (BizEntityDiscoverBali.count>0) {
            
            msg =[[[BpBirthdayResponse alloc] init]autorelease];
            NSMutableArray *BizEntityDiscoverBalitableArray= [[NSMutableArray alloc]initWithCapacity:BizEntityDiscoverBali.count];
            
            for (GDataXMLElement *BizEntityDiscoverBaliElemint in BizEntityDiscoverBali) {
                
                BpBirthday *CgetDiscoverBali =[[BpBirthday alloc]init];
                
                NSArray *DiscoverBaliImageIDArr =[BizEntityDiscoverBaliElemint elementsForName:@"resultCount"];
                if (DiscoverBaliImageIDArr.count >0) {
                    GDataXMLElement *DiscoverBaliImageIDElement =(GDataXMLElement *)[DiscoverBaliImageIDArr objectAtIndex:0];
                    CgetDiscoverBali.resultCount =[DiscoverBaliImageIDElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    //[DiscoverBaliImageIDElement.stringValue intValue];
                }
                
                NSArray *ImagePathArr =[BizEntityDiscoverBaliElemint elementsForName:@"birthday_imgID"];
                if (ImagePathArr.count >0) {
                    GDataXMLElement *ImagePathElement =(GDataXMLElement *)[ImagePathArr objectAtIndex:0];
                    CgetDiscoverBali.birthday_imgID =[ImagePathElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                NSArray *ImageDescriptionArr =[BizEntityDiscoverBaliElemint elementsForName:@"birthday_imgPath"];
                if (ImageDescriptionArr.count >0) {
                    GDataXMLElement *ImageDescriptionElement =(GDataXMLElement *)[ImageDescriptionArr objectAtIndex:0];
                    CgetDiscoverBali.birthday_imgPath =[ImageDescriptionElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                
                
                [BizEntityDiscoverBalitableArray addObject:CgetDiscoverBali];
                [CgetDiscoverBali release];
            }
            msg.birthdayArr=BizEntityDiscoverBalitableArray;
            [BizEntityDiscoverBalitableArray release];
        }
        [doc release];
    }
    return msg;
}

-(XMLResponse*)translateReferenceNo:(NSData *)xmlData
{
    NSError *error;
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:xmlData options:0 error:&error];
    BpInsertNewOrderResponse *msg =nil;
    
    if (doc) {
        NSArray *BizEntityDiscoverBali = [doc.rootElement elementsForName:@"ReferenceNo"];
        if (BizEntityDiscoverBali.count>0) {
            
            msg =[[[BpInsertNewOrderResponse alloc] init]autorelease];
            NSMutableArray *BizEntityReferenceNoArray= [[NSMutableArray alloc]initWithCapacity:BizEntityDiscoverBali.count];
            
            for (GDataXMLElement *BizEntityDiscoverBaliElemint in BizEntityDiscoverBali) {
                BpReferenceNo *CgetBpReferenceNo =[[BpReferenceNo alloc]init];
                CgetBpReferenceNo.ReferenceNumber = [BizEntityDiscoverBaliElemint.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                
                [BizEntityReferenceNoArray addObject:CgetBpReferenceNo];
                [CgetBpReferenceNo release];
            }
            msg.ReferenceArr=BizEntityReferenceNoArray;
            [BizEntityReferenceNoArray release];
        }
        [doc release];
    }
    return msg;
}

//Promotion items
-(XMLResponse*)translateTransactions:(NSData *)xmlData
{
    NSError *error;
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:xmlData options:0 error:&error];
    BpGetAllTransactionResponse *msg =[[[BpGetAllTransactionResponse alloc] init]autorelease];
    
    if (doc) {
        NSArray *BizEntityCheckUp = [doc.rootElement elementsForName:@"BizEntityOrder"];
        if (BizEntityCheckUp.count>0) {
//            msg =[[[BpGetAllTransactionResponse alloc] init]autorelease];
            NSMutableArray *BizEntity= [[NSMutableArray alloc]initWithCapacity:BizEntityCheckUp.count];
            
            for (GDataXMLElement *BizEntityElement in BizEntityCheckUp) {
                BpTransaction *Object_item =[[BpTransaction alloc]init];
                
                NSArray *PickupBranchArr =[BizEntityElement elementsForName:@"PickupBranch"];
                if (PickupBranchArr.count >0) {
                    
                    GDataXMLElement *resultCountDArrElement =(GDataXMLElement *)[PickupBranchArr objectAtIndex:0];
                    Object_item.PickupBranch =[resultCountDArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    NSLog(@"PickupBranch %@", Object_item.PickupBranch);
                }

                NSArray *PickupStateArr =[BizEntityElement elementsForName:@"PickupState"];
                if (PickupStateArr.count >0) {
                    GDataXMLElement *resultCountDArrElement =(GDataXMLElement *)[PickupStateArr objectAtIndex:0];
                    Object_item.PickupState =[resultCountDArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    NSLog(@"PickupState %@", Object_item.PickupState);
                }
                
                NSArray *DeliveryMethodArr =[BizEntityElement elementsForName:@"DeliveryMethod"];
                if (DeliveryMethodArr.count >0) {
                    GDataXMLElement *resultCountDArrElement =(GDataXMLElement *)[DeliveryMethodArr objectAtIndex:0];
                    Object_item.DeliveryMethod =[resultCountDArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                NSArray *CreateDateArr =[BizEntityElement elementsForName:@"CreateDate"];
                if (CreateDateArr.count >0) {
                    GDataXMLElement *resultCountDArrElement =(GDataXMLElement *)[CreateDateArr objectAtIndex:0];
                    Object_item.CreateDate =[resultCountDArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }

                NSArray *ReferenceNoArr =[BizEntityElement elementsForName:@"ReferenceNo"];
                if (ReferenceNoArr.count >0) {
                    GDataXMLElement *resultCountDArrElement =(GDataXMLElement *)[ReferenceNoArr objectAtIndex:0];
                    Object_item.ReferenceNo =[resultCountDArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                NSArray *AmountArr =[BizEntityElement elementsForName:@"Amount"];
                if (AmountArr.count >0) {
                    GDataXMLElement *resultCountDArrElement =(GDataXMLElement *)[AmountArr objectAtIndex:0];
                    Object_item.Amount =[resultCountDArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }

                NSArray *RemarkArr =[BizEntityElement elementsForName:@"Remark"];
                if (RemarkArr.count >0) {
                    GDataXMLElement *resultCountDArrElement =(GDataXMLElement *)[RemarkArr objectAtIndex:0];
                    Object_item.Remark =[resultCountDArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                
                NSArray *PaymentStatusArr =[BizEntityElement elementsForName:@"PaymentStatus"];
                if (PaymentStatusArr.count >0) {
                    GDataXMLElement *resultCountDArrElement =(GDataXMLElement *)[PaymentStatusArr objectAtIndex:0];
                    Object_item.PaymentStatus =[resultCountDArrElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    NSLog(@"PaymentStatus %@", Object_item.PaymentStatus);
                }

                [BizEntity addObject:Object_item];
                [Object_item release];
            }
            msg.TransactionArr=BizEntity;
            [BizEntity release];
        }
        [doc release];
    }
    return msg;
}



@end
