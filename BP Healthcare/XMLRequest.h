//
//  XMLRequest.h
//  KDSWSLayer
//
//  Created by Jermin Bazazian on 12/5/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IXMLRequest.h"
#import "EnumRequestType.h"
#import "GDataXMLNode.h"

@interface XMLRequest : NSObject<IXMLRequest> {
    NSString *webserviceURL;
    NSString *SOAPAction;
    NSString *XMLNameSpace;
    EnumRequestType requestType;
}
@property (nonatomic, retain) NSString *webserviceURL;
@property (nonatomic, retain) NSString *webserviceURL2;
@property (nonatomic, retain) NSString *SOAPAction;
@property (nonatomic, retain) NSString *XMLNameSpace;
@property (nonatomic) EnumRequestType requestType;

-(NSString*) generateHTTPPostMessage;
-(NSString*) generateSOAPMessage;

@end
